\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{5}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Background}{7}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Artificial neural networks}{7}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Overview}{7}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Training}{8}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Forward propagation}{8}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Convolutional Neural Networks}{9}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Overview}{9}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Layers}{9}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Convolutional layer}{9}{section*.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Activation layer}{10}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pooling layer}{10}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Fully connected layer}{11}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Recurrent Neural Networks}{12}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Overview}{12}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}RNN types}{12}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Vanilla RNN}{12}{section*.15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Long Short Term Memory}{13}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Gated Recurrent Unit}{14}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}ResNets}{15}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}DenseNets}{17}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Related work}{21}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Overview}{21}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Re3}{21}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Overview}{21}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Components}{22}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Configurations}{22}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Training}{24}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.5}Results}{25}{subsection.3.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}ROLO}{26}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Overview}{26}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Components}{26}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Configurations}{27}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4}Training}{28}{subsection.3.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.5}Results}{28}{subsection.3.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}RFL}{29}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Overview}{29}{subsection.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Components}{30}{subsection.3.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Configurations}{30}{subsection.3.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.4}Training}{31}{subsection.3.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.5}Results}{31}{subsection.3.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Benchmark Dataset And Protocol}{33}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}OTB}{34}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Overview}{34}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Evaluation metrics}{35}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Success plot}{35}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Fps plot}{36}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Evaluation modes}{37}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}OPE}{37}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}SRE}{37}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}TRE}{37}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Experimental comparison of selected tracking algorithms}{39}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Overview}{39}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}OTB}{39}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}OPE TB100}{39}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}OPE TB50}{41}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Integration of YOLO with OTB}{43}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}YOLO}{44}{subsection.5.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}Initialization error}{45}{subsection.5.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Results}{46}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Qualitative results}{48}{section*.60}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.3}Fixing initialization error}{50}{subsection.5.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Results}{51}{section*.69}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.4}Qualitative result comparison}{52}{subsection.5.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Qualitative results}{52}{section*.72}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bird1}{52}{section*.73}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Boy}{53}{section*.74}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{CarDark}{53}{section*.75}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{David3}{53}{section*.76}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Jump}{53}{section*.77}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Singer2}{54}{section*.78}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Skating1}{54}{section*.79}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Skiing}{54}{section*.80}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Improving Re\textsuperscript {3}}{55}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Overview}{55}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Re\textsuperscript {3} failures}{56}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Experiments}{57}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.1}Retraining Re\textsuperscript {3}}{57}{subsection.6.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.2}Fixed CNN}{58}{subsection.6.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.3}GRU}{59}{subsection.6.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.4}Residual LSTM}{60}{subsection.6.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.5}Dense LSTM}{63}{subsection.6.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.6}Model comparison}{66}{subsection.6.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.7}Results}{66}{subsection.6.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{OPE TB100}{67}{section*.100}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{OPE TB50}{69}{section*.103}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Future works}{71}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Loss change}{71}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Changing the CNN}{73}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Additional datasets}{74}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}No warping}{74}{section.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}Snapshot ensembling}{75}{section.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Conclusions}{77}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Appendix}{79}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Re\textsuperscript {3} failures}{79}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Figures}{89}{appendix*.157}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Tables}{95}{appendix*.158}
