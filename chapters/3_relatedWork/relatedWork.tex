% set image path
\graphicspath{{./chapters/3_relatedWork/images/}}

\chapter{Related work}

\section{Overview}

\section{Re3}
\begin{figure}
    \centering
    \begin{subfigure}{0.7\textwidth}
          \includegraphics[width=\textwidth]{re3.png}
    \end{subfigure} 
    \caption{Network structure of Re\textsuperscript{3}. Source: \cite{Re3}}
\end{figure}

\subsection{Overview}
Re\textsuperscript{3}(Realtime Recurrent Regression network) is a siamese network developed 
for realtime generic object tracking. It uses a convolutional neural network to extract the
visual features from the provided images, a siamese network to directly compare the frames
at subsequent timesteps and a long short term memory to encode and remember the object's 
appearance over time.


At each time $t$ it receives 2 crops: one from the 
frame $f_{t-1}$ and another from the frame $f_t$, both centered in the predicted object's
position at time $t-1$, and they are padded twice the height and width of the predicted box
at time $t-1$ to provide the network with background context and make the tracker more robust 
to fast motion.


Next, a shared CNN extracts the visual features from both crops, while skip layers are used
before the pooling layers to preserve high-resolution spatial information. These outputs are
then fed into a single fully connected layer and later to a LSTM. The network then predicts 
the top left and bottom right corners of the bounding box at time $t$

\subsection{Components}
In particular, the key components of this tracker are:
\begin{itemize}
    \item Siamese network: siamese networks have been shown to be competitive in object tracking.
          They consists of a two identical subnetwork using the same architecture and 
          parameters and they are extremely popular in finding relationships between two things,
          in this case images.
    \item CNN: a convolutional neural network is used to extract the visual feature from the two
          crops. In particular, AlexNet\cite{AlexNet} has been used, which consists of 5  
          convolutional layers, each followed by a max pooling layer.
    \item Skip layers: they are used to preserve the information of lower layers in the CNN, as
          low-level layers can be used to discriminate between objects of the same class, for
          which the high level layer activations are almost equal. This allows both higher 
          accuracy and robustness in the presence of objects of the same class(i.e. a person in 
          the middle of a crowd).
    \item Fully connected layer: its main purpose is to embed the outputs of the CNN and the 
          skip layers, which are then used to feed the LSTM.
    \item LSTM: it is used to preserve and update the encoding of the tracked object over time 
          with a single forward pass.
    \item Regression layer: it is the last layer in the network and its purpose is to output 
          the top left and bottom right corners of the bounding box.
\end{itemize} 

\subsection{Configurations}
In the original paper, a set of different configurations have been tried,
as we can see in the following picture.
\begin{figure}
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{re3Configurations}
      \end{subfigure} 
      \caption{A list of the tested configurations in the Re\textsuperscript{3} paper. Source: \cite{Re3}}
  \end{figure} 
\begin{enumerate}[label=(\Alph*)] 
\item Feed Forward Network (GOTURN): it takes 2 crops as input, one from the previous 
      frame and one in the current frame, centered in the previous frame's predicted 
      bounding box. The 2 crops are then passed to independent CNNs, which extract 
      visual features and pass them to the fully connected layers, where the final 
      prediction of the bounding box is made.
      \begin{figure}
            \centering
            \begin{subfigure}{0.7\textwidth}
                  \includegraphics[width=\textwidth]{goturn}
            \end{subfigure} 
            \caption{A visualization of the GOTURN architecture. Source: \cite{Re3}}
        \end{figure}
\item A + Imagenet Video Training: model A is trained using the large
      dataset of Imagenet video, however the model slightly improves during
      the Imagenet video validation and gets worse in the VOT2014, signaling
      that the model is most likely unable to improve.
\item One layer LSTM: as a result of point B, the model is changed by 
      substituting the 3 fully connected layers of model A with just 1 of
      2048 output and adding a one-layer LSTM with 1024 outputs. However this
      modification causes the model to get worse, as a suitable training 
      protocol has to be defined.
\item C + Self-training: to improve the model C, a specific training protocol
      has been adopted:
      \begin{itemize}
            \item start with few LSTM unrolls and then increase it, to allow the
                  network to first learn simple transformation and later more complex 
                  ones, avoiding fluctuating gradients potentially causing no convergence.
            \item start with ground-truth crops and later use the network's prediction
                  to generate the crops, rather than the ground-truth. This allows 
                  the network to partly recover from its own errors at test time, 
                  since it has already seen it during training.  
            \item at test time reset the LSTM state with the forward pass on the first frame 
                  to avoid the LSTM to drift from values it has never seen, as it has been 
                  trained with sequences of at most 32 frames. This allows the network to 
                  reset from drift and still keep a representation of the object in memory.            
      \end{itemize}
\item D + Simulated Data: to further improve the performance of the LSTM additional
      sequences have been generated: an object to track is selected in an image, 
      and then it is moved to simulate motion, as well as occlusion by taking patches of
      other objects from the same image and trying to partly occlude the tracked object.
      This not only increases the diversity of tracked objects, but also improves the 
      performance of the tracker by teaching it new transformations.
\item E + Skip Layers: skip layers are added to provide the fully connected layers with 
      both the high and low-level visual features: this allows the network to discriminate
      between objects of the same classes, for which the high-level features are the same
      but the low-level are not. As a result it improves the model by a considerable
      margin, particularily improving robustness during occlusion.  
      \begin{figure}[H]
            \centering
            \begin{subfigure}{0.5\textwidth}
                  \includegraphics[width=\textwidth]{Re3Skip}
            \end{subfigure} 
            \caption{A visualization of the model with skip connections(dashed arrows).
                     Source: \cite{Re3}}
      \end{figure}
\item Full Model (F with two LSTM layers): to further improve the model 2 LSTM layers 
      have been used instead of just 1, resulting in an increased accuracy, possibly due 
      to the higher network capacity.
      \begin{figure}[H]
            \centering
            \begin{subfigure}{0.5\textwidth}
                  \includegraphics[width=\textwidth]{re3}
            \end{subfigure} 
            \caption{A visualization of the full model. Source: \cite{Re3}}
      \end{figure}
\item Full Model No ALOV: the model has not been trained using the Amsterdam Library of
      Ordinary Videos dataset.
\item Full Model No Imagenet Video: the model has not been trained using Imagenet Video
      dataset.
\item Full Model No LSTM Reset: in this case the LSTM has not been reset after each 32 
      frames as in point D, resulting in much worse accuracy and robustness, as it 
      accumulates drift over time and is unable to recover from it.
\item Full Model with GoogleNet conv layers: in this last experiment GoogleNet has
      been used to embed the visual appearances of the objects, rather than AlexNet. As 
      a result it provides the best accuracy and robustness, at the expense of the speed, 
      which is cut in half.                        
\end{enumerate}

\subsection{Training}
\label{sect:re3Training}
The training procedure described in the Re3 code consists of several points:
\begin{itemize}
\item datasets: the datasets used are ILSVRC2014 DET for object detection and ILSVRC2017 VID
      for object tracking
\item data augmentation: several techniques are used, such as:
      \begin{itemize}
            \item generation of object tracks from still images: objects are sampled from the 
                  DET dataset and they are moved within the image, as well as being partially
                  occluded by other objects present in the same image
            \item horizontal flipping
            \item random noise generation: the ground truth bounding boxes are scaled and 
                  translated by a tiny amount in each frame of the sequence
            \item mixing network's prediction with ground truth: the labels used for training
                  consist of a mixture of the network's prediction and ground-truth labels, so
                  that the network can learn to recover from its own errors. This parameter
                  is first set to zero so that the network can fully learn to ground truths,
                  but over the course of training is increased along with the time step unrolls.
      \end{itemize}
\item cnn update: at the very beginning of the training the cnn is initialized with the AlexNet's
      weights, and it is later updated as training progresses: this may allow to increase the
      accuracy of the overall tracker, but causes the training to take longer.
\item training curriculum: the training is initialized with 64 batch size, 2 unrolls and 0 probability
      of using the network's prediction for the next timestep crop, and as soon as the loss plateaus 
      the batch size is halved and the unrolls are doubled, up to 32 unrolls, with increasing probability
      of mixing the network's prediction with the ground truth by using increments of 0.25.
\item parameter initialization: non pretrained weights are initialized with the MSRA initialization method,
      whereas the LSTM states are initialized to zero.
\item optimizer: ADAM is used as optimizer, with default momentum and weight decay. As previously stated, 
      its learning rate is initialized to $10^{-6}$ and is later decreased to $10^{-6}$ after 10,000
      iterations.
\item loss function: MAE is used as a loss function, to encourage exact matches, as well as to reduce the
      potential negative effect on training caused by outliers.
\item number of iterations: the whole training process is executed for about 200,000 iterations, taking 
      roughly one week.
\end{itemize} 

\subsection{Results}
\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{re3Results}
      \end{subfigure} 
      \caption{Comparison of trackers using the VOT 2014 and 2016 benchmarking suites: 
               the size of the circle denotes the tracker's framerate, whereas its x and y
               coordinates represent robustness and accuracy, respectively. Source: \cite{Re3}}
\end{figure}
From the results we can see that Re\textsuperscript{3} achieves competitive results with other
state of the art trackers, while runnning significantly faster. In particular, by looking at 
the results on VOT 2016, we can conclude that it achieves the best tradeoff between accuracy,
robustness and framerate, while allowing at the same time multiple object tracking due to its
high framerate in single object tracking.

\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{re3Results1}
      \end{subfigure} 
      \caption{Qualitative results for Re\textsuperscript{3}. Green and red respectively
               represent the ground truth and the model's bounding boxes. Source: \cite{Re3}}
\end{figure}
On the other hand, by looking at some qualitative results we can see that the model successfully
tracks objects with attributes appearance change, deformation and camera motion, while it fails
in sequences presenting similar objects or background clutter, possibly fooling the CNN module
which provides noisy features to the LSTM which as a result loses the tracking of the object.  


\section{ROLO}
\begin{figure}[H]
      \centering
      \begin{subfigure}{0.5\textwidth}
            \includegraphics[width=\textwidth]{rolo}
      \end{subfigure} 
      \caption{Network structure of ROLO. Source: \cite{ROLO}}
\end{figure}

\subsection{Overview}
ROLO(Recurrent YOLO) is a type of recurrent realtime tracker, which uses YOLO both for 
extracting visual features and the object's detections. The visual features and the detections
are then provided to an LSTM module, which maintains and updates its internal encoding of the
tracked object and regresses the coordinates of the tracked object in the current frame.

\subsection{Components}
\begin{figure}
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{roloDetailed}
      \end{subfigure} 
      \caption{A more in depth network visualization of ROLO. Source: \cite{ROLO}}
\end{figure}
In short, the key components of ROLO are:
\begin{itemize}
      \item Traditional CNN: at time $t$ it receives the frame $f_t$ and extracts the visual 
            features which are then combined in a fully connected layer, providing as output
            a vector of size 4096.
      \item Detection: YOLO has been used as a detection module, receiving the 4096 vector
            as input and providing as output a vector of size $S \times S \times (B \times
             5+C)$, encoding the
            region predictions. The formula states that the image has been divided into 
            $S \times S$ 
            splits, each of which has $B$ bounding box predicted, represented by the 5 location
            parameters $x, y, w, h and c$, plus a one-hot class label vector of size $C$,
            representing the class label of a bounding box. In ROLO the values selected are 
            $S=7, B=2, C=20$.
      \item Heatmap: in order to know what happens during occlusions the detection information
            from YOLO is converted into a vector of length 1024, which is then translated into 
            32x32 heatmap allowing to visualize intermediate results. 
      \item Tracking: the tracking module consists of a LSTM which receives as input both the
            visual features extracted from the traditional CNN and the information about the
            detections from the detection module. Finally the LSTM is used to regress the 
            final bounding box for the tracked object.
\end{itemize}

\subsection{Configurations}
\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{roloResults} 
      \end{subfigure} 
      \caption{Success plots of OPE on 3 experiments: (a) LSTM trained on 22 videos of OTB-30
               and tested on the rest 8 clips, (b) LSTM has been trained with 1/3 of the videos
               in OTB-30 and tested on the whole sequence frames, (c) LSTM has been trained with 
               1/3 ground truths, but with all sequence frames. Source: \cite{ROLO}}
\end{figure}
First, 3 experiments have been performed to evaluate the regression and memory capabilities
of the LSTM:
\begin{enumerate}
      \item the LSTM has been trained on 22 videos and tested on the 8 remaining clips. The
            results indicate that the LSTM is well suited for regression, although this
            training curriculum does not seem well suited for LSTm training.
      \item the LSTM has been trained with 1/3 of the frames as well as their ground truths 
            and has been tested over the whole sequence frames. This training routine allowed 
            the model to improve, possibly due to similar dynamics at training and testing time.
      \item the LSTM has been trained with 1/3 ground truths, but with all sequence frames. This
            further improves the model, %TODO add
\end{enumerate}
The results clearly show the regression capabilities of the LSTM, as well as the need to train 
it with similar dynamics to the testing.% TODO explain better

\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \centering
            \includegraphics[width=0.8\textwidth]{roloConfigurations} 
      \end{subfigure} 
      \caption{Average intersection over union scores and framerate at different step sizes.
               Source: \cite{ROLO}}
\end{figure}
Different configurations have been tried according to the parameter step size: it denotes the
number of previous frames considered each time by the LSTM for a given prediction. By 
performing experiments at step sizes of 1, 3, 6 and 9 it was determined that a step size of 
6 achieves the best tradeoff between accuracy and framerate.

Other configurations have been tried, such as dropout on visual features, random offset of the
detections during training and an alternative cost function to emphasize detection over visual
features, but they were not included in the paper as they provided inferior results.

\subsection{Training} 
Model training has been divided into 3 phases:
\begin{enumerate}
      \item CNN pre-training: in this phase a convolutional neural network is trained to learn
            the visual features
      \item YOLO training: the YOLO model has been trained for object detections using the 
            Imagenet dataset and then it is finetuned on the VOC dataset, allowing it to 
            detect objects belong to 20 classes.
      \item LSTM training: in the last phase the LSTM has been trained for object tracking. 
            It has been trained on a subset of the OTB dataset %TODO complete
\end{enumerate}

\subsection{Results}

\begin{figure}
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{roloResults} 
      \end{subfigure} 
      \caption{Success plots of OPE on 3 experiments. Source: \cite{ROLO}}
\end{figure}
To train the LSTM, different variations have been tried:
\begin{enumerate}
      \item the LSTM has been trained on 22 videos and tested on the 8 remaining clips. The
            results indicate that the LSTM is well suited for regression, although this
            training curriculum does not seem well suited for LSTm training.
      \item the LSTM has been trained with 1/3 of the frames as well as their ground truths 
            and has been tested over the whole sequence frames. This training routine allowed 
            the model to improve, possibly due to similar dynamics at training and testing time.
      \item the LSTM has been trained with 1/3 ground truths, but with all sequence frames. This
            further improves the model, %TODO add
\end{enumerate}

(a) LSTM trained on 22 videos of OTB-30
and tested on the rest 8 clips, (b) LSTM has been trained with 1/3 of the videos
in OTB-30 and tested on the whole sequence frames, (c) LSTM has been trained with 
1/3 ground truths, but with all sequence frames.

\begin{figure}
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{roloResults1}
      \end{subfigure} 
      \caption{Average intersection over union scores and framerate at different step sizes.
               Source: \cite{ROLO}}
\end{figure}

\section{RFL}
\begin{figure}[H]
      \centering
      \begin{subfigure}{0.6\textwidth}
            \includegraphics[width=\textwidth]{rfl}
      \end{subfigure} 
      \caption{Network structure of Recurrent Filter Learning. Source: \cite{RFL}}
\end{figure}

\subsection{Overview}
Recurrent Filter Learning(RFL) is based on the concept of using a convolutional LSTM rather
than a traditional one: in the former the cell and the hidden states consist of matrices,
resembling the features of the CNN, whereas in the latter vectors are used: this allows the 
LSTM to learn and adapt an image filter according to the tracked object, rather than only
rely on a flattened vector of features, allowing it to preserve and correctly update the
spatial informations.

At a given timestep $t$ the E-CNN extracts visual features from an exemplar image. Next, the 
convLSTM memorizes and updates the tracked object's appearance by relying on its previous 
states $h_{t-1}$ and $c_{t-1}$ as well as the object's exemplar feature map $e_t$. Then an
object filter $f_t$ is generated by passing the hidden state $h_t$ through a convolutional 
layer and it is convolved with the feature maps provided by the S-CNN on a serch image to 
generate a response map used to locate the target within the current frame.

\subsection{Components}
the key components of RFL are:
\begin{itemize}
      \item E-CNN: this CNN is used to extract the visual features from the object exemplar
            used for generating the target filter.
      \item S-CNN: it extracts a feature map from the search image, which is used to produce
            the response map by convolving it with the generated filter.
      \item convLSTM: it consists of a standard LSTM architecture, with the only difference
            being that $W_f, W_i, W_e, W_o$ are convolutional filter weights of size 3x3x1024.
            $W_l$ is instead the convolutional weight of the output layer of size 1x1x256. It 
            mantains and updates the apperance of the tracked object using its previous cell
            and hidden states, $c_{t-1}$ and $h_{t-1}$, as well as the visual features extracted
            by the E-CNN. The convolutional nature of its inputs allows it to mantain the spatial
            information, which would be otherwise lost as it is the case with standard LSTM, where
            the features need to be flattened before being provided as input.
            \begin{figure}
                  \centering
                  \begin{subfigure}{0.6\textwidth}
                        \includegraphics[width=\textwidth]{convLstm}
                  \end{subfigure} 
                  \caption{The architecture of convolutional LSTM. Source: \cite{RFL}}
            \end{figure}
\end{itemize}
More in detail, the architecture of E-CNN and S-CNN is the same, but parameters are not shared
since the features needed to generate the target filter could be different from the ones 
needed to distinguish the target from the background.
\begin{figure}
      \centering
      \begin{subfigure}{0.6\textwidth}
            \includegraphics[width=\textwidth]{rflCnn}
      \end{subfigure} 
      \caption{The cnn architecture used for both E-CNN and S-CNN. Source: \cite{RFL}}
\end{figure}

\subsection{Configurations}
Three different network variations have been explored:
\begin{itemize}
      \item RFL-shared: sharing the CNN weights between the E-CNN and S-CNN. 
      \item RFL-no-init-net: initializing the memory states to all zeros instead of 
            initializing them with the output of the initialization network.
      \item RFL-norm-LSTM: using a standard LSTM rather than a convolutional LSTM, this has
            been obtained by setting the filter size of the convLSTM to 1x1.
\end{itemize}

\begin{figure}[H]
      \centering
      \begin{subfigure}{0.6\textwidth}
            \includegraphics[width=\textwidth]{rflConfigurations}
      \end{subfigure} 
      \caption{OPE on OTB100 results for different configurations of RFL. Source: \cite{RFL}}
\end{figure}
From the result it is clear that sharing parameters between the E-CNN and S-CNN significantly
reduces the performance, whereas using a standard LSTM over a convolutional one provides
slightly worse results, in the order of 10\%.

\subsection{Training}
To learn from video sequences, the ILSVRC dataset has been used: the training curriculum is:
\begin{itemize}
      \item select a video sequence
      \item uniformly sample $N+1$ frames from the chosen video: frames from 1 to $N$ are 
            used to generate the object exemplars, whereas frames from 2 to $N+1$ are used
            as search images.
      \item crop both the object exemplars and search images around the center of groundtruth 
            bounding box, with size of the crop being twice the target size for the object 
            exemplar and the search image being four times: this is needed to provide the
            network with context. 
\end{itemize}

Moreover, several types of data augmentation have been used:
\begin{itemize}
      \item random color variation, such as brightness, contrast, hue and saturation.
      \item random translation and stretching to both exemplar and search images
\end{itemize}

\subsection{Results}
\begin{figure}
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{rflResults}
      \end{subfigure} 
      \caption{Success plots for OPE OTB100 on 6 challenging attributes. Source: \cite{RFL}}
\end{figure}
From the benchmarking performed on OPE of OTB100 we can see that RFL performs particularily 
well on sequences with attributes scale variation, out of view, in plane rotation, 
illumination varitation, background clutter and fast motion: this is most likely due RFL 
better adapting its tracking filter to distinguish between tracked object and background. 

\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{rflResults1}
      \end{subfigure} 
      \caption{Qualitative results for RLF and other trackers. Source: \cite{RFL}}
\end{figure}
From the qualitative results we can instead observe how RFL performs better than the other
trackers in sequences with fast motion, background clutter and illumination variation, where 
most of other trackers fail.