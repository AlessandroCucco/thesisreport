% set image path
\graphicspath{{./chapters/4_benchmarkDatasetAndProtocol/images/}}

\chapter{Benchmark Dataset And Protocol}
In order to evaluate the results of the proposed trackers we needed a benchmark suite, 
consisting of both the code and data, currently the most used are:
\begin{itemize}
    \item OTB(Object Tracking Benchmark) \cite{WuLimYang13}: it consists of up to 100 different sequences and it is aimed for 
          the tracking of generic objects. Moreover it provides difficult sequences such as 
          illumination variation, camera movement and zoom, partial or complete occlusion,
          deformation, etc. It reports the result using the area under curve graph by taking 
          into account not only temporal but also spatial robustness.
    \item VOT(Visual Object Tracking) \cite{VOT_TPAMI}: it is similar to OTB, however the main
          differences are that it resets the tracker on failure and reports the result as both 
          accuracy and robustness plots.
    \item KITTI(Karlsruhe Institute of Technology and Toyota Technological Institute) 
          \cite{Geiger2012CVPR}: it consists of 21 training and 29 test sequences.
          However only the classes 'Car' and 'Pedestrian' are evaluated, and as such it is
          best suited for autonomous driving testing, rather than generic object tracking.
    \item MOT(Multiple Object Tracking) \cite{MOTChallenge2015} \cite{MOT16}: it is 
          aimed at evaluating trackers for multiple object tracking, primarily for objects 
          of class 'pedestrian' and 'vehicle' and in difficult condistions such as occlusion,
          moving camera and different weather conditions.
\end{itemize}
We decided to focus our attention on OTB since it consists of sequences of different object 
classes and as such is best suited for generic object tracking, as well as providing the code 
in python, for easier debugging and execution.

\newpage
\section{OTB}
\subsection{Overview}
\begin{figure}
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{OTBSequences.png} 
      \end{subfigure}
      \caption{An example of the sequences present in the OTB benchmark. Source: \cite{OTB}}
\end{figure}
The testing suite contains up to 100 different sequences belonging to 
different classes such as:
\begin{itemize}
    \item person
    \item face
    \item bird
    \item car
    \item board
    \item box
    \item can
    \item dog
    \item doll
    \item fish
    \item bottle
    \item bike
    \item panda
    \item rubik cube
    \item tiger
    \item toy
    \item transformer
    \item sylvester
\end{itemize}
For each sequence a set of data is provided:
\begin{itemize}
    \item the ground-truth file, containing the list of ground-truth 
          bounding boxes for all frames of the sequence
    \item the individual frames of the whole sequence, saved as a jpg image
    \item an attribute file, containing the attributes of the sequence
    \item a config file used to keep some metadata such as start and end 
          frame, sequence name.
\end{itemize}
Moreover, each sequence can be assigned to one or more attributes:
\begin{itemize}
\item Illumination Variation - the illumination in the target region 
      is significantly changed.
\item Scale Variation - the ratio of the bounding boxes of the first 
      frame and the current frame is out of the range [1/ts, ts], 
      ts \textgreater 1 (ts=2).
\item Occlusion - the target is partially or fully occluded.
\item Deformation - non-rigid object deformation.
\item Motion Blur - the target region is blurred due to the motion of
      target or camera.
\item Fast Motion - the motion of the ground truth is larger than tm 
      pixels (tm=20).
\item In-Plane Rotation - the target rotates in the image plane.
\item Out-of-Plane Rotation - the target rotates out of the image plane.
\item Out-of-View - some portion of the target leaves the view.
\item Background Clutters - the background near the target has the 
      similar color or texture as the target.
\item Low Resolution - the number of pixels inside the ground-truth 
      bounding box is less than tr (tr=400).
\end{itemize}

\section{Evaluation metrics}
Since we want to evaluate the performance of the trackers, some objectively measurable metrics
have to be defined. In particular for a tracker we are interested into:
\begin{itemize}
      \item Accuracy: the degree of similarity between the predicted and ground truth labels. 
            It is calculated as the intersection over union(IOU) between the predicted and 
            ground truth bounding boxes.
      \item Robustness: the ability of the tracker of not losing the tracked object. A higher 
            value indicates that a tracker rarely loses sight of the object.
      \item Framerate: the computational complexity of the algorithm. Since we want trackers 
            to operate in realtime, a value of at least 30 frames per second is required.
\end{itemize}
Moreover in order to visualize these informations at different threshold values, different graphs
are used:
\begin{itemize}
      \item Success plot
      \item Fps plot
\end{itemize}

\subsection{Success plot}
\begin{figure}
      \centering
      \begin{subfigure}{0.5\textwidth}
            \includegraphics[width=\textwidth]{successPlot.png} 
      \end{subfigure}
      \caption{An example of success plot. On the x axis the threshold value, on the y axis 
      the percentage of frames whose intersection over union is higher than the threshold}
\end{figure}
The success plot is used to establish which tracker provides the best accuracy at different
threshold values. Ideally we want a straight horizontal line of the form $y=100$
For each tracker, the following protocol is followed:
\begin{enumerate}
      \item define a treshold value $t$.
      \item compute the intersection over union(IOU) between the tracker’s and the 
            ground-truth bounding boxes.
      \item calculate the percentage between the number of frames whose overlap is higher 
            than the threshold $t$ and the total number of frames.
      \item repeat point 1-3 for various threshold values in the range (0, 1].
      \item rank the trackers according to their area under curve(AUC).
\end{enumerate}
The main advantage of the success plot is that it allows to have a
broad picture of the tracker’s performance in terms of accuracy: it 
allows to compare the trackers at different IOU tresholds and highlight
the best according to the overall accuracy, although it does not provide other important
information such as the tracker's robustness and computational complexity.

\subsection{Fps plot}
\begin{figure}
      \centering
      \begin{subfigure}{0.5\textwidth}
            \includegraphics[width=\textwidth]{fpsPlot.png} 
      \end{subfigure}
      \caption{An example of fps plot. On the x axis the tracker's name, on the y axis 
      the average framerate over all test sequences}
\end{figure}
The original OTB code does not compare trackers according to their speed: to tackle this issue
we implemented a new procedure to compare different trackers according to their runtime speed, 
in order to indirectly calculate their computational complexity. We call the result of this
protocol the frames per second(FPS) plot, which displays the average tracking speed of a given
tracker. Since we want real time tracking, a value of at least 30 FPS is required.
For each tracker, the following protocol is followed:
\begin{enumerate}
      \item compute the average framerate for each sequence.
      \item compute the average of the previous point to get the average fps
            for all sequences.
\end{enumerate}
It allows to compare the trackers in terms of frames per second and
is best used in conjunction with the success plot to evaluate the 
overall performance of a tracker.

\section{Evaluation modes}
In order to establish the performance of each tracker, a set of evaluation modes are
available, each taking into account different aspects, such as long-term, initialization
and temporal tracking robustness. To test these aspects of the tracker OPE, SRE and TRE 
are used respectively.
Note that in our testing we included only OPE, as the provided code at \cite{OTBCode} 
did not correctly work for SRE and TRE.

\subsection{OPE}
One Pass Evaluation(OPE) follows the following protocol:
\begin{enumerate}
      \item initialize the tracker in the first frame with the ground-truth bounding box.
      \item run the tracker up to the end of the sequence.
      \item repeat the 2 previous points for all testing sequences.
      \item report the success and fps plot for each attribute.
\end{enumerate}
While simple to implement, it has 2 major drawbacks:
\begin{itemize}
      \item a tracker may be sensitive to initialization in the first 
            frame, so that its performance may change related to a 
            different initial bounding box
      \item most trackers are not able to recover from failures, so the 
            reported bounding box are meaningless when a tracker encounters 
            a failure
\end{itemize}

\subsection{SRE}
Spatial Robustness Evaluation(SRE) follows the following protocol:
\begin{enumerate}
      \item slightly scale or translate the initial ground-truth bounding box
      \item initialize the tracker with the output of the previous point
      \item run the tracker up to the end of the sequence
      \item repeat point 1-3 N different times, where N is the number of 
            different initializations you want to use
      \item report the average precision or success rate of all results
\end{enumerate}
It allows to verify if a tracker is sensitive to initialization, as well 
as counteract possible labeling errors in the initial ground-truth
bounding box.

\subsection{TRE}
Temporal Robustness Evaluation(TRE) follows the following protocol:
\begin{enumerate}
\item split the sequence into different temporal windows
\item initialize the tracker at the beginning of each temporal 
      window with the corresponding ground-truth bounding box and 
      let it run until the end of the sequence
\item report the average precision or success rate of all results
\end{enumerate}
The main advantage of TRE over OPE is that it allows to evaluate the tracker
also on those frames where it would have failed due to tracking failure.