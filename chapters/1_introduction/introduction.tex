% set image path
\graphicspath{{./chapters/1_introduction/images/}}

\chapter{Introduction}
Object tracking is a recurrent problem in the current world: 
from autonomous driving, surveillance, traffic control, robotics, just
to name a few. In particular, realtime generic object tracking is 
still an open challange, as the provided system must be both 
computationally efficient and able to adapt to a variety of objects,
most of which it has not been trained on. 
Traditional trackers milestones such as STRUCK \cite{STRUCK}, TLD
\cite{TLD} and KCF\cite{KCF} approach the problem with a variety of 
methods: from hand crafted features to online learning, as well as a 
combination of the two.

Recently, however, deep learning algorithms have shown extremely 
competitive results, thanks to their ability to automatically learn
complex non linear transformation, as well as scaling their accuracy 
with the number of examples, differently from other methods. As such,
they have been applied to a variety of fields, such as natural language
processing, computer vision, medicine, speech recognition.
Deep learning methods have been successfully applied to image 
classification, where deep architectures such as AlexNet \cite{AlexNet}
and VGG \cite{VGG} have shown state of the art results,  %TODO continue

Trackers, on the other hand, not only use use a CNN to extract visual features, but some
of them incorporate a RNN for online learning and updating of the object appearance without
needing for a backpropagation phase, resulting in faster execution. 

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.7\textwidth}
          \includegraphics[width=\textwidth]{trackersClassification}
    \end{subfigure} 
    \caption{Trackers classification. Source: \cite{TrackingReview}.}
\end{figure}


Generally, trackers use a combination of 2 neural networks:
\begin{itemize}
    \item CNN(Convolutional Neural Network) to extract visual features from the frames, as 
          it has achieved great success in object classification.
    \item RNN(Recurrent neural network) to remember the tracked object's apperance over time,
          being able to remember previous states and as a result being best suited for sequence
          modelling.
\end{itemize}

In particular, trackers can be divided into three main categories, based on their network 
structure:
\begin{itemize}
    \item CNN-based: a CNN is used to extract visual features and develop a robust appearance
          model of the tracked object. Moreover it can be further subdivided into:
          \begin{itemize}
              \item CNN-C(Convolutional Neural Network - Classification): this type of network 
                    is trained to distinguish the tracked object from its surrounding by 
                    using a binary classifier.
              \item CNNN-M(Convolutional Neural Network - Matching): it focuses on learning
                    a robust similarity function in order to correctly match the object 
                    template within a search region.
          \end{itemize}
    \item RNN-based: a RNN is used, since it is best suited for sequence modelling, as its
          output can be re-applied to it in the next timestep. Their usage in object tracking is motivated
          by the great success of RNNs in handwriting and speech recognition, where they achieved state of
          the art results. 
    \item other: other types of networks such as autoencoders are used.
\end{itemize}

Next, trackers can also be classified according to their usage of the deep networks: 
\begin{itemize}
    \item FEN(Feature Extraction Networks): these types of networks use the deep networks only to 
          extract visual features, which are later used by a traditional method to learn an 
          appearance model and thus locate the model. Moreover they can be further divided 
          according to how many layers are used for the feature extraction process:
          \begin{itemize}
              \item FEN-SL(Feature Extrraction Networks - Single Layer): these types of networks 
                    use the features resulting from a single layer.
              \item FEN-ML(Feature Extraction Networks - Multiple Layers): they use the features 
                    from different layers of the networks, and have 
                    have shown better result than FEN-SL, as they are capable of using the features
                    coming from different layers, each providing a different abstraction level, 
                    where previous layers have more precise location information and low level visual
                    information, whereas later layers are more coarse in location, but capture more
                    semantic information.
          \end{itemize}
    \item EEN(End to End Networks): these networks instead use the deep networks not only for feature
          extraction, but also to evaluate possible candidates, by using one out of probability
          map, heat map, object position, candidate score or bounding box. In particular, they can 
          be further divided into 3 categories, according to their outputs:
          \begin{itemize}
              \item EEN-S(End to End Networks - Score): trackers build a sequence of candidates by 
                    using either a particle filter or a sliding window, then producing the scores of 
                    these candidates, so that the tracked object can be finally localized.
              \item EEN-M(End to End Networks - Map): these networks, on the other hand, build a 
                    confidence, probability, response or heat map, and then use other methods to 
                    localize the object
              \item EEN-B(End to End Networks - Box): they directly produce either the bounding box 
                    or the location of the tracked object by learning an end to end network.
          \end{itemize}   
\end{itemize}

Lastly, trackers can be further divided according to their tracking procedure, which is critical for 
developing a robust and accurate tracker, depending on how the model has been trained:
\begin{itemize}
    \item NP-OL(No Pre-trained - Online Learning): these types of trackers are not pre-trained, and
          instead learn to track the object by online training. One main disadvantage of these methods
          is that they are not able to achieve a high enough framerate to be used for realtime tracking.
    \item IP-NOL(Image Pre-trained - No Online Learning): a possible solution for improving the tracker 
          is to pretrain it offline on image data, to learn a robust and accurate feature extraction model.
          These trackers allow a faster tracking, as the underlying model is not updated at testing time.
    \item IP-OL(Image Pre-trained - Online Learning): to further improve the performance of the tracker, a
          solution is to train it not only offline, but online as well, so that it can better adapt to the
          tracked object, resulting in improved accuracy and robustness, at the expense of tracking speed.
    \item VP-NOL(Video Pre-trained - No Online Learning): one of the main pitfalls of previous methods is 
          they train the tracker on image data, rather than on video sequences. As such, the simplest and
          most effective solution to improve the tracker is to train it on videos, although the training 
          data is far more limited than image datasets.
    \item VP-OL(Video Pre-trained - Online Learning): to futher improve the tracker's accuracy and robustness,
          online learning can be applied on top of offline learning from video sequences, resulting in the 
          tracker to better adapt to the tracked object's change in appearance, at the cost of tracking speed.
\end{itemize}


