\graphicspath{{./chapters/6_experiments/images/}}

\chapter{Improving Re\textsuperscript{3}}

\section{Overview}
This chapter is divided into 2 main parts. In the first one we will identify the
tracking failures of Re\textsuperscript{3}, by looking at some qualitative example as
support, to better understand which changes in the network may result in an improvement.
In the second part, instead, we will discuss the experiments that have been performed
by changing the architecture of Re\textsuperscript{3}. In particular we proposed a set 
of changes to the original architecture to improve the final model and reduce training time, 
such as:
\begin{itemize}
    \item Retraining the Re\textsuperscript{3} model.
    \item Fixing the CNN.
    \item Substituting the LSTM with the GRU.
    \item Changing the RNN architecture by using a residual LSTM. 
    \item Changing the RNN architecture by using a dense LSTM.
\end{itemize}


All experiments have been performed on a machine with the following configuration:
\begin{itemize}
	\item CPU: Intel® Core™ i7-8700 CPU @ 3.20GHz × 12
	\item GPU: GeForce GTX 1080
	\item RAM: 31,4 GiB
	\item OS: Ubuntu 18.04.02 LTS
\end{itemize}

Moreover, the libraries and frameworks used are:
\begin{itemize}
	\item Python 2.7+ or 3.5+
	\item Tensorflow 1.5, installed through pip by using the command pip install tensorflow-gpu==1.5.0
	\item NumPy, installed through pip
	\item OpenCV 2, installed through pip
	\item CUDA 9.0, installed by using the runfile available on the nvidia website
	\item CUDNN 7.0.5, installed by using the runfile available on the nvidia website
\end{itemize}

Experiments have been performed by using as a baseline the code provided by \cite{Re3} in the gitlab 
repository available at \cite{Re3Code}, which has been later modified to test the various network 
configurations.

\section{Re\textsuperscript{3} failures}

\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{bird1}
    \end{subfigure} 
	\caption{Tracking failure for the OTB sequence "bird1". Failure is 
			 caused by the cloud compleately occluding the target, causing 
			 the tracker not being able to recover.}
	\label{fig:re3FailBird1}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{blurCar4}
    \end{subfigure} 
	\caption{Tracking failure for the OTB sequence "blurCar4". Failure is 
			 caused by the combination of fast camera motion and motion blur, causing the 
			 tracker to latch onto a similar object.}
	\label{fig:re3FailBlurCar4}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{crowds}
    \end{subfigure} 
	\caption{Tracking failure for the OTB sequence "crowds". The tracker fails to follow 
			 the correct target, since during the partial occlusion phase it latches onto 
			 a similar
			 object, possibly due to both the low resolution of the image, as well as the 
			 extremely similar shape and appearance of the occluder and occluded, causing
			 the CNN to output extremely similar activations, which fool the following LSTM
			 module.}
	\label{fig:re3FailCrowds}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{human6}
    \end{subfigure} 
	\caption{Tracking failure for the OTB sequence "human6". Tracking failure is due to the 
			 complete occlusion, causing the tracker to latch onto the occluder and lose
			 track of the target.}
	\label{fig:re3FailHuman6}
\end{figure}

By looking at Re\textsuperscript{3}'s tracking failures in figures \ref{fig:re3FailBird1},
\ref{fig:re3FailBlurCar4}, \ref{fig:re3FailCrowds} and \ref{fig:re3FailHuman6} we noted 
that most of them are caused by:
\begin{itemize}
\item Complete occlusion: in this case the whole object's shape is occluded by another 
      object, causing the tracker to fail also in the subsequent frames since it is 
      unable to recover from failure. A possible solution may be the inclusion of a 
      detector in the tracking system, which allows to reinitialize the tracker once 
      the target has been lost, although it requires a detector able to recognize
      not only the class of the tracked object but its specific appearance.
\item Suboptimal features: in some of the sequences the tracker fails to follow the 
      correct object, being fooled by either similar objects or by background clutter.
      A possible solution of this may be the usage of a more accurate convolutional
      pipeline, at the expense of the tracker's speed.
\item LSTM reset: as previously stated, the LSTM state is reset every 32 frames with the 
      output of the forward pass on the first frame. This allows the model to avoid drift,
      but at the same time causes it to forget the model's transformation in those frames,
      causing reduced accuracy and robustness. A possible solution may be the usage of a
      detector which proposes candidates and resetting the LSTM state with the forward pass 
      on the most likely candidate, although it requires a detector able to recognize the 
      class of the tracked object and as such it cannot be applied to unknown object 
      tracking.
\item Fast motion: in very few of the testing sequences the tracker seems to lose the
      object possibly due to the fast motion of this, causing the crop in the next frame
      not to fully enclose the tracked object, resulting either in failure or error 
      accumulation in the LSTM layers because of the incorrect or incomplete visual features
	  coming from the CNN pipeline.
\item suboptimal LSTM: we suggest the current LSTM architecture may be unable to correctly 
	  follow and maintain a suitable representation of the tracked object and successfully
	  track it in all frames, as from the sequences available in appendix \ref{appendix:re3Failures}
	  it is clear that it fails in if the target is close to a similar object or 
	  motion blur happens or the object undergoes heavy deformation. 
\end{itemize} 
A complete list of Re\textsuperscript{3}'s tracking failure is available in 
the appendix \ref{appendix:re3Failures}.


\section{Experiments}
In this section we will discuss about the experiments we performed, by both explaining the 
motivation and the achieved results, as well as the modifications made to the original model.

\subsection{Retraining Re\textsuperscript{3}}
As a first experiment we decided to retrain Re\textsuperscript{3} with the original architecture
and training curriculum detailed in \ref{sect:re3Training}: in particular we started with an
initial learning rate of $10^{-5}$, which was later decreased to $10^{-6}$ and was then kept
constant until the end of the training at around 200,000 iterations. This experiment allowed us to both 
get a baseline for the following experiments, as well as verifying that the results of the paper
\cite{Re3} were reproducible. 
From the training procedure we immediately noticed the loss decreased extremely slow, as it can be 
seen in figure \ref{fig:re3RetrainedLoss}.

\begin{figure}[H]
	\centering
	\begin{subfigure}{\textwidth}
		  \includegraphics[width=\textwidth]{re3RetrainedLoss}
	\end{subfigure} 
	\caption{A visualization of the training loss for the model Re\textsuperscript{3} Retrained.
		   On the horizontal and vertical axes the number of training steps and the training
		   loss, respectively. Notice how the loss decreases more slowly and is higher 
		   compared to the residual and dense model}	
	\label{fig:re3RetrainedLoss}
\end{figure}

Once the training finished, we evaluated both the retrained and original versions of 
Re\textsuperscript{3} on the ILSVRC2014 VID validation set: the results, available in table 
\ref{tab:re3Retrained} clearly show that the results of \cite{Re3} are not reproducible even
though, to the best of my knowledge, I followed their guidelines. The retrained model not only
is less robust, as we can see from the high number of lost targets, but is also less accurate, 
given the lower mean IOU value. As such, we suggest the authors of Re\textsuperscript{3} did
not fully disclose their training curriculum and more investigation is needed to correctly 
pinpoint the sources of this difference.

\begin{table}[H]
	\centering
	\begin{tabular}{ |c|c|c|c| } 
		\hline
		Tracker & Lost targets & Mean IOU & Number of parameters \\
		\hline
		Re\textsuperscript{3} & 243 & 0.72 & 85699686 \\
		\hline
		Re\textsuperscript{3} Retrained & 350 & 0.64 &  85699686 \\
		\hline
	\end{tabular}
	\caption{Testing results between the network of \cite{Re3}, Re\textsuperscript{3}, and 
			 our densely connected LSTM, both tested on the ILSVRC2014 VID validation set.
			 Lost targets denotes the 
			number of times the overlap between the ground truth and predicted bounding boxes
			is 0, whereas the mean IOU is computed as the mean intersection over union between
			the 2 bounding bounding boxes. Number of parameters denotes instead the total 
			number of trainable parameters of the model.}
	\label{tab:re3Retrained}
\end{table}

\subsection{Fixed CNN}
One of the main problems with Re\textsuperscript{3} training protocol is that the CNN is 
first initialized with the AlexNet weights, but it is later finetuned. This has several effects,
such as:
\begin{itemize}
    \item longer training time: since the gradients have to be propagated back to the CNN layers
          its parameters have to be updated via gradient descent, causing the training to take 
          longer.
    \item forgetting about object categories: the CNN may forget about some of the object 
          categories it was trained to classify, as the tracking datasets may contain only a 
          subset of these.
    \item increased robustness in motion blur or low resolution: since the tracking videos
          contain a lot of motion blur or low resolution images, the CNN may improve in these
          scenarios.
\end{itemize}

The trained model is the same used in Re\textsuperscript{3} \cite{Re3}.

The training process has been implemented in the same way as in \ref{sect:re3Training},
with the only difference being that the weights of each convolutional layer in the CNN have 
been fixed. A visualization of the training loss is available in figure \ref{fig:re3FixCnnLoss}:
the model has been trained for only 75k iterations, as its loss was unable to improve over time.

\begin{figure}[H]
	\centering
	\begin{subfigure}{\textwidth}
		  \includegraphics[width=\textwidth]{re3FixCnnLoss}
	\end{subfigure} 
	\caption{A visualization of the training loss for the model Re\textsuperscript{3} fixCNN.
		   On the horizontal and vertical axes the number of training steps and the training
		   loss, respectively. Notice how the loss quickly plateaus at relatively high loss 
		   values, and is unable to decrease, even if the number of unrolls is increased.}	
	\label{fig:re3FixCnnLoss}
\end{figure}

We tested the network by fixing the weights of the pretrained AlexNet CNN and compared the final 
results with the original Re\textsuperscript{3} model a summary is available in table 
\ref{table:fixCnn}.
\begin{table}[H]
	\centering
	\begin{tabular}{ |c|c|c|c| } 
		\hline
		Tracker  & Lost targets & Mean IOU & Number of parameters \\
		\hline
		Re\textsuperscript{3} Retrained & 350 & 0.64 &  85699686 \\
		\hline
		Re\textsuperscript{3} fixCNN & 440 & 0.63 & 85699686 \\
		\hline
	\end{tabular}
	\caption{Comparison of the results between Re\textsuperscript{3} and Re\textsuperscript{3}
			 with fixed CNN on the Imagenet VID validation set. Lost targets denotes the 
			 number of times the overlap between the ground truth and predicted bounding boxes
			 is 0, whereas the mean IOU is computed as the mean intersection over union between
			 the 2 bounding bounding boxes. Number of parameters denotes instead the total 
			 number of trainable parameters of the model.}
	\label{table:fixCnn}
\end{table}
As we can see from the results, the performance of Re\textsuperscript{3} with fixed CNN is worse
than the original. We suggest this is due to a variety of reasons, such as:
\begin{itemize}
    \item image warping: as already explained before, in Re\textsuperscript{3} crops are warped
          to a fixed sized image of size 227x227. This causes a large appearance change for very
          tall and thin or very thick and short objects, and as a result the pretrained CNN may
          be unable to recognize these objects, causing the resulting features to be noisy and, 
          as a consequence, the LSTM module cannot properly learn.
    \item motion blur and low resolution: the finetuned CNN may better adapt to motion blur and 
          low resolution images, since they are almost always present in tracking videos.
    \item higher trainable parameter count: finally a possible explanation of the difference
          between the two architectures may be the lower total trainable parameters in the fixed
          CNN, causing the model to have less total capacity than its trainable CNN counterpart.
\end{itemize}

\subsection{GRU}
In the second experiment we decided to substitute the LSTM with the GRU for several reasons:
\begin{itemize}
    \item faster training and convergence: since the GRU contains less parameters than the
          LSTM it should allow us to speed up training 
    \item same or better results: several papers in the architecture such as \cite{LSTMvsGRU}
          propose evidence that the GRU can sometimes achieve the same or better results as the
          LSTM.
    \item faster tracking: since the GRU contains less parameters than the LSTM it should also 
          allow us to achieve a faster tracking speed.
\end{itemize}
The testing has been performed by replacing the LSTM module with the GRU and following the same 
training protocol described in section \ref{sect:re3Training}, we will refer to Re\textsuperscript{3} and 
Re\textsuperscript{3}-GRU as the original and GRU version of Re\textsuperscript{3}, respectively.
\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{gru}
    \end{subfigure} 
    \caption{Comparison of training loss for Re\textsuperscript{3} and Re\textsuperscript{3}-GRU
             in light blue and magenta, respectively. Notice how the training loss of 
             Re\textsuperscript{3}-GRU plateaus, whereas Re\textsuperscript{3} improves over time.}
	\label{fig:gru}
\end{figure}
As we can see from figure \ref{fig:gru}, Re\textsuperscript{3}-GRU seems unable to adapt to the training examples, 
causing its loss to plateau at around 1, whereas Re\textsuperscript{3} fits to the training examples,
allowing it to further decrease its loss. As such, we suggest the GRU is unable to achieve the 
same results as the LSTM for object tracking, possibly due to the LSTM being better at capturing
long-term dependencies thanks to both its forget and update gate, whereas the GRU is limited by 
its update gate used for both forgetting and updating new features.  

\subsection{Residual LSTM}
Inspired to deep convolutional networks we decided to modify the RNN architecure by making it
deeper rather than wider, as in literature \cite{VGG} this has shown to 
achieve better results than their shallow counterparts.

However, making the network deeper is not enough, as the authors of \cite{ResNet50} have experimentally
shown. As such, inspired by the results of residual networks depicted in section \ref{sect:resNets}, 
we decided to implement a residual connection for LSTMs similar 
to \cite{ResLSTM}, in order to evaluate its effectiveness in object tracking. Our residual
LSTM block consists of two LSTMs connected in series, whose output is summed with their 
input, as we can see in figure \ref{fig:resLstmBlock}.
\begin{figure}[H]
      \centering
      \begin{subfigure}{0.5\textwidth}
            \includegraphics[width=\textwidth]{resLstmBlock}
      \end{subfigure} 
      \caption{Example of a residual LSTM block: it consist of 2 cascaded LSTM whose output is added to the first 
			  LSTM input.}
	  \label{fig:resLstmBlock}
\end{figure}

The resulting network is equivalent to Re\textsuperscript{3}, with the exception of:
\begin{itemize}
      \item fc6: the fully connected layer at the end of the siamese network is set to use 768 hidden 
            units rather than 1024. This is needed since its and the LSTM's outputs are summed, and without this
            change we should either set the LSTM units to 1024, causing the model to have too many parameters, or 
		downscale fully connected features to an appropiate size. We decided to opt for 768 hidden units since 
		it allowed us to get a model with roughly the same number of parameters as Re\textsuperscript{3}, 
		to have a fair comparison between the models. Moreover we added a batch normalization layer 
		after this layer, as it allowed us to fix its activations in  the $(-1, 1)$ range, allowing us to achieve 
		both higher accuracy and faster convergence, as explained in figure \ref{fig:activations}. 
      \item resBlocks: 3 residual LSTM blocks are added to the network in place of Re\textsuperscript{3}'s LSTM blocks,
			each of which is composed by 2 LSTM of 768 units, for the same reasons cited above. As LSTM, we used the 
			layerNormBasicLSTMCell implementation of tensorflow with default parameters.As a consequence, the
            resulting network has roughly the same number of parameters.
\end{itemize}
A visualization of the complete network is available in figure \ref{fig:resRe3}

\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{resRe3}
      \end{subfigure} 
      \caption{Visualization of the used architecture: each resBlock corresponds to the previously defined residual 
               LSTM block. Note that the convolutional part of the network, denoted by the conv blocks, as well as
			   the big\_concat block, is the same as in the original Re\textsuperscript{3} model.}
	  \label{fig:resRe3}
\end{figure}

The training procedure is equivalent to Re\textsuperscript{3}, as previously cited in section
\ref{sect:re3Training}, with few minor differences:
\begin{itemize}
	\item we started with an initial learning rate of $10^{-4}$, which was later decreased to $10^{-5}$ and $10^{-6}$
		  as we noted a plateau in the loss function.
	\item we used learning rate scaling of $10^{-1}$ for convolutional layers, as they were initialized to AlexNet's
		  weights, and as such they only needed to be finetuned.
\end{itemize}

A visualization of the training loss is available in figure \ref{fig:resLstmLoss}. Nothe how 
the loss quickly converges to a stable value, differently from Re\textsuperscript{3} Retrained.
\begin{figure}[H]
	\centering
	\begin{subfigure}{\textwidth}
		  \includegraphics[width=\textwidth]{resLstmLoss}
	\end{subfigure} 
	\caption{A visualization of the training loss for the proposed residual LSTM architecture.
			 Note how the loss quickly converges to a stable value.}
	\label{fig:resLstmLoss}
\end{figure}

\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \centering
            \includegraphics[width=0.4\textwidth]{lstmOut}
      \end{subfigure} 
      \begin{subfigure}{\textwidth}
            \centering
            \includegraphics[width=0.4\textwidth]{fc6NoBatchNorm}
      \end{subfigure} 
      \begin{subfigure}{\textwidth}
            \centering
            \includegraphics[width=0.4\textwidth]{fc6BatchNorm}
      \end{subfigure} 
      \caption{Visualization of the activations of the different layers: the x axis represents the value, the y axis
               represents the frequency of that value, the z axis represents time. From top to bottom the activations
		   	   of LSTM, fc6 without batch normalization, fc6 with batch normalization. Note how the activations
			   between the 2 versions of fc6 differ with respect to the outputs of the LSTM.}
	  \label{fig:activations}
\end{figure}

A comparison between Re\textsuperscript{3} Retrained and our proposed residual LSTM model on the Imagenet VID validation set 
is available in table \ref{tab:resLstm}. As we can see from the results, our proposed method slightly improves both the 
tracker's robustness and accuracy, possibly due to:
\begin{itemize}
	\item deeper network: as for CNNs, deeper LSTM networks perform better than shallow ones.
	\item residual connections: residual connections allow for easier gradient flow through the network and faster training
	\item batch normalization: batch normalization applied to the fc6 layer allows to fix the activation of this layer in the
		  $(-1, 1)$ range, allowing subsequent layers to learn more easily, as its activations do not wildly vary.
	\item higher parameter count: the higher parameter count may allow the model to have higher capacity and, as a result, to 
		  obtain better results over Re\textsuperscript{3}'s network.
\end{itemize}

\begin{table}[H]
	\centering
	\begin{tabular}{ |c|c|c|c| } 
		\hline
		Tracker & Lost targets & Mean IOU & Number of parameters \\
		\hline
		Re\textsuperscript{3} Retrained & 350 & 0.64 &  85699686  \\
		\hline
		residual LSTM & 303 & 0.66 & 87716712 \\
		\hline
	\end{tabular}
	\caption{Testing results between the original network of \cite{Re3}, 
			Re\textsuperscript{3}, and our residual LSTM, both tested on the ILSVRC2014 
			VID validation set. Lost targets denotes the 
			number of times the overlap between the ground truth and predicted bounding boxes
			is 0, whereas the mean IOU is computed as the mean intersection over union between
			the 2 bounding bounding boxes. Number of parameters denotes instead the total 
			number of trainable parameters of the model.}
	\label{tab:resLstm}
\end{table}

As a result, our proposed network provides a marginal improvement over Re\textsuperscript{3}'s network, and we suggest more 
experiments are needed to correctly weight the benefits of each component in the proposed network.


\subsection{Dense LSTM}
Inpired by the results of densely connected networks depicted in section \ref{sect:denseNets},
we decided to implement a dense block, made up of LSTMs: each LSTM layer
in the block receives as input both the output of the fully concatenated layer, as well as 
the output of all the previous LSTMs layers. A visualization of the proposed block is available at 
\ref{fig:denseLSTM}.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.6\textwidth}
		  \includegraphics[width=\textwidth]{denseLSTM}
	\end{subfigure} 
	\caption{Visualization of the proposed dense LSTM block: each LSTM layer receives as
			 input both the output of the fully connected layer, as well as the output of
			 all previous LSTMs.}
	\label{fig:denseLSTM}
\end{figure}

The resulting architecture is equivalent to the one used in Re\textsuperscript{3} \cite{Re3}
for what concerns the skip and convolutional layers, while we changed:
\begin{itemize}
    \item fc6: its size has been set to 900 units, to achieve a similar parameter count as 
		  Re\textsuperscript{3}'s model. Moreover we added batch normalization in this 
		  layer to speed up and stabilize training.
 	\item RNN: we changed the whole RNN part of the tracker, by using a single dense LSTM 
		  block with 4 LSTM layers, each of 512 units.For what concerns the LSTM, we used
		  the layerNormBasicLSTMCell implementation of tensorflow, with default parameters.
\end{itemize}
A complete view of the model is available in figure \ref{fig:denseLSTMNet}.

\begin{figure}[H]
	\centering
	\begin{subfigure}{\textwidth}
		  \includegraphics[width=\textwidth]{denseLstmNet}
	\end{subfigure} 
	\caption{A visualization of the proposed dense LSTM architecture.}
	\label{fig:denseLSTMNet}
\end{figure}

The training process has been performed following the guidelines of Re\textsuperscript{3} 
\cite{Re3}, with some minor differences:
\begin{itemize}
	\item We started training with an initial learning rate of $10^{-4}$, which was later 
		  decreased to $10^{-5}$ and $10^{-6}$ as we noticed a plateau in the loss function.
	\item We used a learning rate scaling of $10^{-1}$ with respect to the current learning 
		  rate for convolutional layers, as these layers only needed to be finetuned.
	\item Once we reached a number of unrolls of 32, we set the probability of using the 
		  network's prediction to crop the image first to 0, and later it is increased 
		  to 1 by increments of 0.25 each time the loss plateaus.
\end{itemize}
A visualization of the training is available in figure \ref{fig:denseLstmLoss}. Note how the 
loss quickly converges to a stable values, differently from Re\textsuperscript{3} Retrained,
whose loss decreased extremly slowly, requiring more training steps. 

\begin{figure}[H]
	\centering
	\begin{subfigure}{\textwidth}
		  \includegraphics[width=\textwidth]{denseLstmLoss}
	\end{subfigure} 
	\caption{A visualization of the training loss for the dense LSTM architecture. Note how 
			 the loss quickly converges to a stable value, differently from Re\textsuperscript{3}
			 Retrained}
	\label{fig:denseLstmLoss}
\end{figure}

We tested both our proposed network and Re\textsuperscript{3} on the ILSVRC2014 VID validation
set, whose results are available in table \ref{tab:denseLstm}. From the results we suggest that:
\begin{itemize}
	\item Our proposed dense LSTM network significantly outperforms Re\textsuperscript{3}
		  Retrained, both in terms of robustness and accuracy.
	\item Deeper dense LSTM architectures are superior to shallow ones.
	\item Batch normalization allows for faster and more stable training	
\end{itemize}

\begin{table}[H]
	\centering
	\begin{tabular}{ |c|c|c|c| } 
		\hline
		Tracker & Lost targets & Mean IOU & Number of parameters \\
		\hline
		Re\textsuperscript{3} Retrained & 350 & 0.64 &  85699686  \\
		\hline
		dense LSTM & 258 & 0.68 & 87031408 \\
		\hline
	\end{tabular}
	\caption{Testing results between the network of \cite{Re3}, Re\textsuperscript{3}, and 
			 our densely connected LSTM, both tested on the ILSVRC2014 VID validation set.
			 Lost targets denotes the 
			number of times the overlap between the ground truth and predicted bounding boxes
			is 0, whereas the mean IOU is computed as the mean intersection over union between
			the 2 bounding bounding boxes. Number of parameters denotes instead the total 
			number of trainable parameters of the model.}
	\label{tab:denseLstm}
\end{table}

In conclusion, dense LSTM provides a superior tracking performance over Re\textsuperscript{3}
Retrained, even though more experiments are needed to correctly weight the benefits of model
parameters, such as network depth, number of units within each LSTM cell and usage of the
batch normalization layer.


\subsection{Model comparison}

\begin{table}[H]
	\centering
	\begin{tabular}{ |c|c|c|c| } 
		\hline
		Tracker & Lost targets & Mean IOU & Number of parameters \\
		\hline
		Re\textsuperscript{3} & 243 & 0.72 & 85699686 \\
		\hline
		Re\textsuperscript{3} Retrained & 350 & 0.64 &  85699686 \\
		\hline
		Re\textsuperscript{3} fixCNN & 440 & 0.63 & 85699686 \\
		\hline
		residual LSTM & 303 & 0.66 & 87716712 \\
		\hline
		dense LSTM & 258 & 0.68 & 87031408 \\
		\hline
	\end{tabular}
	\caption{Comparison of the results of the different experiments on the ILSVRC2014 VID 
		   validation set. 
		   Lost targets denotes the 
			number of times the overlap between the ground truth and predicted bounding boxes
			is 0, whereas the mean IOU is computed as the mean intersection over union between
			the 2 bounding bounding boxes. Number of parameters denotes instead the total 
			number of trainable parameters of the model.}
	\label{tab:modelComparison}
\end{table}

The comparison between the different experiments, all tested on the ILSVRC2014 VID validation 
set, is available in table \ref{tab:modelComparison}. From the results we can conclude that:
\begin{itemize}
	\item The retrained version of Re\textsuperscript{3} performs worse than its original
		counterpart. As already stated before, we suggest this is due to the authors not 
		fully disclosing their training procedure.
	\item The fixed CNN version of Re\textsuperscript{3}, Re\textsuperscript{3} fixCNN, 
		performs worse than Re\textsuperscript{3} Retrained: we suggest this is due to the
		CNN being unable to adapt to the warping of the image performed by Re\textsuperscript{3}
		before feeding it to the CNN.
	\item residual LSTM performs better than Re\textsuperscript{3} Retrained, possibly due to 
		the deeper residual network being able to capture more complex transformations, and 
		the batch normalization layer, which allows to have a stable and fast learning.
	\item dense LSTM performs even better than the residual LSTM model, by requiring 
		slightly less parameters. This is most likely due to the fact that each LSTM layer 
		can access the output of all previous LSTM layers, allowing each layer to better 
		adapt to changes in the previous layers, as well as thanks to the easier gradient
		flowing through the network.
\end{itemize}

From these result we can conclude that both residual and dense LSTM models provide a significant
improvement over Re\textsuperscript{3}'s architecture, possibly due to their deeper network, as 
well as due to the usage of the batch normalization layer.
However, more experiments are needed to properly weight the contributions of other aspects of 
these models, such as:
\begin{itemize}
	\item The usage of a different learning rate for the pretrained layers and the 
		initialized ones.
	\item The addition of the batch normalization layer.
	\item The usage of the layer normalized version of the LSTM.
\end{itemize}

\subsection{Results}
In this section we compare the results between our best model, dense Re3, and other state of the
art recurrent trackers, RFL and the original model of Re\textsuperscript{3}, all tested on the 
OTB benchmarking suite.

\subsubsection{OPE TB100}
\begin{figure}[H]
    \centering 
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{OPE_TB100_1} 
        \includegraphics[width=\textwidth]{OPE_TB100_2}
		\includegraphics[width=\textwidth]{OPE_TB100_3}
		\includegraphics[width=\textwidth]{OPE_TB100_4}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \begin{subfigure}{\textwidth}
    \ContinuedFloat
    \centering        
        \includegraphics[width=\textwidth]{OPE_TB100_5}
        \includegraphics[width=\textwidth]{OPE_TB100_6}
    \end{subfigure}
	\caption{Results for various attributes of Re\textsuperscript{3}, RFL and dense Re\textsuperscript{3}
			 using OPE 
			 TB100. The trackers have been run on a configuration with i7 2600, 8GB DDR3 1333 MHz, GTX 1060 3GB.}ù
	\label{fig:exp_OPE_TB100}
\end{figure}

As we can see from figure \ref{fig:exp_OPE_TB100}, denseRe\textsuperscript{3}, our proposed model, performs similarly to
both Re\textsuperscript{3} and RFL. In particular, it significantly outperforms Re\textsuperscript{3} in low resolution, 
occlusion and out of view sequences, while performing equally or marginally worse in all other attributes. RFL, on the
other hand, still outperforms both other trackers in almost all attributes, although as we already stated before it 
runs significantly slower.


\subsubsection{OPE TB50}
\begin{figure}[H]
    \centering 
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{OPE_TB50_1} 
        \includegraphics[width=\textwidth]{OPE_TB50_2}
		\includegraphics[width=\textwidth]{OPE_TB50_3}
		\includegraphics[width=\textwidth]{OPE_TB50_4}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \begin{subfigure}{\textwidth}
    \ContinuedFloat
    \centering        
        \includegraphics[width=\textwidth]{OPE_TB50_5}
        \includegraphics[width=\textwidth]{OPE_TB50_6}
    \end{subfigure}
	\caption{Results for various attributes of dense Re\textsuperscript{3} and other state of the art trackers
			 using OPE 
			 TB50. The trackers have been run on a configuration with i7 2600, 8GB DDR3 1333 MHz, GTX 1060 3GB.}
	\label{fig:exp_OPE_TB50}
\end{figure}

As we can see from the results in figure \ref{fig:exp_OPE_TB50}, denseRe\textsuperscript{3} performs similarly to
other state of the art trackers. It perform particularly well in low resolution, occlusion and out of view sequences,
where it reaches the top 4 positions. In all other sequences it performs worse that Re\textsuperscript{3}, although
it is able to reach the top 5 positions for all attributes.