% set image path
\graphicspath{{./chapters/5_benchmark/images/}}

\chapter{Experimental comparison of selected tracking algorithms}

\section{Overview}
In this chapter we will evaluate the performance of 3 recurrent trackers:
Re\textsuperscript{3}, RFL and ROLO.
This benchmark was needed for 2 main reasons: first, because the papers
provided results based on different datasets, and as such they could not 
be directly compared, secondly object tracking using a recurrent neural
network is a fairly new topic in research, and as a consequence no public 
benchmark, to the best of my knowledge, is available for these trackers 
in the literature.

This chapter is divided into 2 main parts. In the first one we will
evaluate the performance of the trackers Re\textsuperscript{3} and RFL
according to the benchmarking suite OTB100, showing the results for all
provided attributes in the benchmark. Note that ROLO was tested on this
suite as it was trained  In the second part we will instead 
modify the original OTB benchmark so that YOLO can be used as a detector 
to provide the trackers Re\textsuperscript{3}, RFL and ROLO with the 
initial bounding box, rather than relying solely on the ground truth 
initialization. This procedure allows us to analize the behaviour of 
the trackers in case of
imperfect initialization, as well as providing a testing methodology as 
close as possible to real life, where the initial bounding box 
may be provided by either a detector or by imperfect human labeling.

\section{OTB}
First of all we downloaded the github repository containing the python code of the Online 
Tracking Benchmark(OTB) suite \cite{OTBCode}: it contains the scripts needed to run the
various experiments as well as the data used to evaluate the trackers, counting up to 100
different sequences. Moreover are present the results of other top-level trackers, evaluated 
using the tb50 test suite, which can be used to compare new trackers.

\subsection{OPE TB100}
Here are listed the result for Re\textsuperscript{3} and RFL using the one pass evaluation
test and using the whole 100 sequences.
\begin{figure}[H]
    \centering 
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{OPE_TB100_0}
        \includegraphics[width=\textwidth]{OPE_TB100_1}
        \includegraphics[width=\textwidth]{OPE_TB100_2}
        \includegraphics[width=\textwidth]{OPE_TB100_3}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \ContinuedFloat
    \centering
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{OPE_TB100_4}
        \includegraphics[width=\textwidth]{OPE_TB100_5}
        \includegraphics[width=\textwidth]{OPE_TB100_6}
    \end{subfigure} 
    \caption{Results for various attributes of Re\textsuperscript{3} and RFL using OPE 
             OTB100. The trackers have been run on a configuration with i7 2600, 8GB DDR3
             1333 MHz, GTX 1060 3GB.}
    \label{fig:OPE_TB100}
\end{figure}
From the results in Figure \ref{fig:OPE_TB100} we can conclude that:
\begin{itemize}
    \item The best overall tracker is RFL, which is $\approx 6.5\%$ more accurate than 
          Re\textsuperscript{3}, at the cost of being $\approx 115\%$ slower.
    \item RFL performs particularly well in background clutter, low resolution, occlusion
          and out of view sequences, possibly thanks to its convolutional LSTM dynamically
          adjusting its convolutional filter to better represent the tracked object, whereas 
          Re\textsuperscript{3} suffers from the flattening of the convolutional activations
          fed into its LSTM layers.
    \item both trackers perform simarly in deformation, fast motion, in plane rotation, 
          illumination variation, motion blur, out of plane rotation and scale variation,
          however RFL still performs better than Re\textsuperscript{3}, even if by a
          negligible margin.
\end{itemize}

\subsection{OPE TB50}
In this section we present the results obtained on TB50, a subset of the OTB benchmark,
consisting of up to 50 different sequences.

\begin{figure}[H]
    \centering 
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{OPE_TB50_0}
        \includegraphics[width=\textwidth]{OPE_TB50_1} 
        \includegraphics[width=\textwidth]{OPE_TB50_2}
        \includegraphics[width=\textwidth]{OPE_TB50_3}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \begin{subfigure}{\textwidth}
    \ContinuedFloat
    \centering
        \includegraphics[width=\textwidth]{OPE_TB50_4}
        \includegraphics[width=\textwidth]{OPE_TB50_5}
        \includegraphics[width=\textwidth]{OPE_TB50_6}
    \end{subfigure}
    \caption{Results for various attributes of Re\textsuperscript{3} and RFL using OPE 
             OTB50. The trackers have been run on a configuration with i7 2600, 8GB DDR3 1333 MHz, gtx 1060 3GB.}
\end{figure}
As we can see from the results, RFL provides a more accurate tracking,
at the cost of being slow. On the other hand Re3 still provides a good
accuracy, while still being able to process frames at more than realtime speed, possibly allowing
for multiple object tracking.
Note that results of other trackers appear, too, since they were provided with the OTB benchmark code,
and can be used to compare both Re\textsuperscript{3} and RFL with other state-of-the-art trackers.

%\subsection{OPE TB30}
%\begin{figure}[H]
%    \centering 
%    \begin{subfigure}{\textwidth}
%        \includegraphics[width=\textwidth]{OPE_TB30_0}
%        \includegraphics[width=\textwidth]{OPE_TB30_1}
%        \includegraphics[width=\textwidth]{OPE_TB30_2}
%        \includegraphics[width=\textwidth]{OPE_TB30_3}
%    \end{subfigure}
%\end{figure}
%\begin{figure}[H]
%    \ContinuedFloat
%   \centering
%   \begin{subfigure}{\textwidth}
%        \includegraphics[width=\textwidth]{OPE_TB30_4}
%        \includegraphics[width=\textwidth]{OPE_TB30_5}
%        \includegraphics[width=\textwidth]{OPE_TB30_6}
%    \end{subfigure} 
%    \caption{Results for various attributes of Re\textsuperscript{3} and RFL using OPE 
%             OTB100. The trackers have been run on a configuration with i7 2600, 8GB DDR3 1333 MHz, gtx 1060 3GB.}
%end{figure}

\section{Integration of YOLO with OTB}
The major drawback of the considered trackers is that they require 
the ground-truth initialization to track an object: this can be easy
to use in benchmarks, as well as in real life, as the initialization
is given by a human, but can become a serious drawback in automated
scenarios.

To solve this problem, we decided to use an object detector, which 
can be used to detect objects in the initial frames and then select 
the one we want to track by keeping the highest IOU object between 
all locations and the ground-truth bounding box.

\subsection{YOLO}
The structure of YOLO is depicted in Figure \ref{fig:YOLO_Net}:
\begin{figure}
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloModel}
    \end{subfigure}
    \caption{An example of the YOLO architecture. Source: \cite{ROLO}}
    \label{fig:YOLO_Net}
\end{figure}

It works in the following way:
\begin{enumerate}
\item Traditional CNN:  a set of convolutional and maxpool layers 
      extract the visual features from the image, which are then
      flattened into a 4096 feature vector
\item Connected layer: the 4096 feature vector of point 1 is fed 
      into a fully connected layer which outputs a 7x7x30 vector
\end{enumerate}
The output of point 2 contains the detections of the objects in the
current image, and it is structured in a vector of dimension $S \times S \times (B \times 5+C)$
meaning that:
\begin{itemize}
\item the image has been divided into $S \times S$ splits
\item each split contains $B$ predicted bounding boxes represented by
      5 location parameters $(x, y, w, h)$ which are relative to the
      image size(i.e. they are in the range [0, 1])
\item a one-hot encoding of the class label of length $C$ is also 
      predicted, indicating the class label of each bounding box
\end{itemize}
As detector we used the fast-YOLO model, a stripped down version of 
the original YOLO, less robust but able to achieve higher framerates.
In addition we set the values of the output vector to $S=7, B=2, C=20$.

An example of the YOLO detection is provided in figure \ref{fig:yoloDetection}: note that
in this particular case the detection is extremely accurate, although this is not always 
the case, as the detector may fail in providing a bounding box, or it may be substantially
inaccurate, causing the tracking module to fail due to the incorrect initialization.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{yoloDetection}
    \end{subfigure}
    \caption{Example of {\color{blue}YOLO detection} and {\color{green}ground truth}: note
             how in this particular case the detection is extremely accurate.}
    \label{fig:yoloDetection}
\end{figure}

\subsection{Initialization error}
Since we wanted to test the trackers initialized by a detector, we
decided to use YOLO to provide the initial bounding box to the tracker:
we followed this protocol when integrating the detector and the tracker:
\begin{enumerate}
    \item we skip all the frames where YOLO does not detect any object
        or if the IOU of the YOLO detections and the ground truth 
        bounding box is 0.
    \item we select the highest IOU detection of YOLO with the 
          ground-truth bounding box and use it to initialize the tracker.
    \item we run the tracker on the following frames.
\end{enumerate}

After this process is repeated for each tracker, we generate the same
plots used in the standard OTB in order to compare the trackers.

For what concerns ROLO, one of the main disadvantages of this model is that, differently
from other trackers, it does not use the predicted location at time t
to predict the new location at time t+1, but uses the bounding box 
coming from YOLO at each timestamp, which is computed as the highest 
IOU bounding box  between the ground truth and the predicted bounding
box from the YOLO output. As such, it requires the knowledge of the
ground-truth at each frame and cannot be used in unsupervised scenarios.
To solve this problem we decided to use the predicted bounding box 
at time t as the yolo’s bounding box at time t+1, by substituting the
x, y, w and h values in the 6 element vector coming from the yolo
bounding box.
An additional problem is that the model, differently from the paper’s 
specifications, has been trained by not setting to zero the elements 
c and p in the 6 element vector coming from the detection. As a result,
during tracking we had to feed these values into the LSTM as well, 
otherwise it caused the model to drift and not follow the object being 
tracked.
Another important parameter is the step size: it denotes the number of 
previous frames considered each time for a prediction by LSTM. The best
results are achieved with a step size of 6 frames, but we decided to 
use a step of 1 frame as the other trackers required only 1 frame for
initialization and it would have been unfair to provide ROLO with 6 
frames and 6 ground-truth bounding boxes.
For what concerns the model, we decided to use the one with step size
of 1 as stated before, but it has also been trained on a subset of 
tb30, the test set, and as such the results may be biased.

Note that however the tracker’s accuracy and robustness highly depend
on the detector’s bounding box, as a wrong/incorrect initialization 
may lead the tracker to a early failure. Moreover as the detector is
used to initialize the tracker, a subset of sequences from OTB have 
been selected, to be sure that the detector is able to recognize the 
objects belonging to these classes.

\subsubsection{Results}
\begin{figure}[H]
    \centering 
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_1} 
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_2}
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_3}
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_4}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \begin{subfigure}{\textwidth}
    \ContinuedFloat
    \centering
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_5}
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_6}
        \includegraphics[width=\textwidth]{yolo_OPE_TB30_7}
    \end{subfigure}
    \caption{Results for various attributes of Re\textsuperscript{3}, RFL and ROLO using OPE 
             OTB30 with YOLO detection as initial bounding box. The trackers have been run on a configuration 
             with i7 2600, 8GB DDR3 1333 MHz, gtx 1060 3GB.}
    \label{fig:OpeTb30}
\end{figure}
As we can see from the results available in figure \ref{fig:OpeTb30} RFL is still the best tracker out of 
the 3, even if ROLO has been trained on 1/3 of the sequences present 
in the tb30 test suite.
Note that the fps result of ROLO is calculated only on the LSTM part
of it, without considering the feature extraction process occurring
in the CNN.
Overall Re\textsuperscript{3} peforms extremely similarly to RFL, while being
at the same time able to achieve a higher framerate. This is possibly due to
its training procedure, where the bounding boxes where randomly scaled and 
translated by a tiny amount.


\subsubsection{Qualitative results}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloBird1}
    \end{subfigure}
    \caption{Example of incorrect detection for the OTB sequence "Bird1": the detector is likely
             fooled by the smaller bird in the background, as well as by the motion blur 
             occurring on the target's wings. Legend: {\color{blue}detector}, 
             {\color{green}ground truth}.}
    \label{fig:yoloBird}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloBoy}
    \end{subfigure}
    \caption{Example of inaccurate detection for the OTB sequence "Boy": the detector's
             bounding box encloses the whole shape of the person as it has been trained to 
             recognize a person and not a face. Legend: {\color{blue}detector}, 
             {\color{green}ground truth}.}
    \label{fig:yoloBoy}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloCarDark}
    \end{subfigure}
    \caption{Example of incorrect detection for the OTB sequence "CarDark": the detector's
             bounding box is compleately off the target, as it has been possibly fooled by
             the low resolution of the image, as well as by the inusual dark background.
             Legend: {\color{blue}detector}, {\color{green}ground truth}.}
    \label{fig:yoloCarDark}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloDavid3}
    \end{subfigure}
    \caption{Example of incorrect detection for the OTB sequence "David3": the detector 
             identifies the car behind the target, for which it provides a bounding box,
             whereas the target, the person in the foreground, is compleately ignored.
             Legend: {\color{blue}detector}, {\color{green}ground truth}.}
    \label{fig:yoloDavid3}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloJump}
    \end{subfigure}
    \caption{Example of inaccurate detection for the OTB sequence "Jump": the detector outputs
             a bouding box almost as large as the entire image. This behaviour is probably 
             caused by the heavy motion blur occurring in the picture.
             Legend: {\color{blue}detector}, {\color{green}ground truth}.}
    \label{fig:yoloJump}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloSinger2}
    \end{subfigure}
    \caption{Example of inaccurate detection for the OTB sequence "Singer2": the detector
             fails to individuate the correct target most likely due to the mostly black
             environment, as well as due to the backround clutter.
             Legend: {\color{blue}detector}, {\color{green}ground truth}.}
    \label{fig:yoloSinger2}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloSkating1}
    \end{subfigure}
    \caption{Example of incorrect detection for the OTB sequence "Skating1": the detector's
             bounding box is compleately wrong, possibly due to the inusual pose of the 
             skater as well as to the dark environment.   
             Legend: {\color{blue}detector}, {\color{green}ground truth}.}
    \label{fig:yoloSkating1}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{yoloSkiing}
    \end{subfigure}
    \caption{Example of incorrect detection for the OTB sequence "Skiing": the detector 
             fails to individuate the correct target, possibly due to the inusual appearance
             of the person.  
             Legend: {\color{blue}detector}, {\color{green}ground truth}.}
    \label{fig:yoloSkiing}
\end{figure}

\subsection{Fixing initialization error}
As seen in the figures \ref{fig:yoloBird}, \ref{fig:yoloBoy}, \ref{fig:yoloCarDark}, 
\ref{fig:yoloDavid3}, \ref{fig:yoloJump}, \ref{fig:yoloSinger2}, \ref{fig:yoloSkating1}
and \ref{fig:yoloSkiing}, the detector fails in individuating
the correct object in the frame, thus providing an incorrect starting 
bouding box, leading the tracker to an early failure. To solve this
problem, we calculated the detector’s initial bounding box following
this protocol:
\begin{itemize}
\item filter all the detections coming from YOLO according to the 
      tracked object’s class (i.e. only consider person detections
      if we are interested in this class)
\item filter the remaining detections according to a threshold of 
      0.23 applied on the detection’s confidence in order to ignore
      unsure detections
\item keep the highest IOU detection with the ground-truth bounding
      box and use it to initialize the tracker
\end{itemize}
Using this protocol we were able to discard all those detections which
provided a wrong intial bounding box and it allowed to initialize the
tracker with a rectangle as close to the ground truth as possible.

Unfortunately because of the filter process many detections have been
discarded, and as a result the initialization of the tracker is often 
performed later in the sequence. Moreover in some sequences, as we 
can see later, the detector fails compleately to initialize the tracker
as no suitable bounding boxes have been found. As a consequence, the 
trackers are correctly initialized, but the overall metrics are lower, than the previous 
experiment, due to the initialization being performed later in the sequence or not being
performed at all, in case of some particularly difficult examples.

\subsubsection{Results}
\begin{figure}[H]
    \centering 
    \begin{subfigure}{\textwidth}
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_0} 
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_1}
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_2}
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_3}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \begin{subfigure}{\textwidth}
    \ContinuedFloat
    \centering
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_4}
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_5}
        \includegraphics[width=\textwidth]{yoloFix_OPE_TB30_6}
    \end{subfigure}
    \caption{Results for various attributes of Re\textsuperscript{3}, RFL and ROLO using OPE 
             OTB30 with fixed YOLO as detector for the initial bounding box. The trackers have been run on a configuration 
             with i7 2600, 8GB DDR3 1333 MHz, gtx 1060 3GB.}
    \label{fig:fixYoloOpeTb30}
\end{figure}

From the results in figure \ref{fig:fixYoloOpeTb30} we can see that 

\subsection{Qualitative result comparison}
In this section we will provide the reader with the qualitative comparison between the 
initialization modes discussed in the previous sections.

\subsubsection{Qualitative results}
Here we will compare the bounding boxes of {\color{blue}yolo} and of the
{\color{green}ground truth} in those sequences where yolo previously 
failed in detecting a suitable box.

Legend: left = previous chapter YOLO initialization, right = new 
init method

\subsubsection{Bird1}
In this sequence the new intialization method provides a better 
initial bounding box, even if it happens some frames later than 
the left one.
\begin{center}
\includegraphics[scale=0.35]{bird1Comparison}
\end{center}

\subsubsection{Boy}
In this case the new initialization method provides the same bounding
box as before.
\begin{center}
\includegraphics[scale=0.35]{boyComparison}
\end{center}    

\subsubsection{CarDark} 
In this example the new method does not provide an initial bounding
box as the detections confidence is not high enough.
\begin{center}
\includegraphics[scale=0.35]{carDarkComparison}
\end{center} 

\subsubsection{David3}
In this sequence the new method provides a much better bounding box, 
as the filtering phase allows to discard the car’s detection occurring 
in the left example and the threshold allows to retain only the most
confident detections, at the cost of a later tracker initialization.
\begin{center}
\includegraphics[scale=0.35]{david3Comparison}
\end{center} 

\subsubsection{Jump}
In this sequence the initial bounding box is more accurate in the 
right example, at the cost of being almost at the end of the sequence.
\begin{center}
\includegraphics[scale=0.35]{jumpComparison}
\end{center} 

\subsubsection{Singer2}
In this example the difference is minimal, possibly due to the 
background clutter and the black environment fooling the detector.
\begin{center}
\includegraphics[scale=0.35]{singer2Comparison}
\end{center} 

\subsubsection{Skating1}
In this sequence the detector provides a much better initial bounding
box even if it happens some frames later.
\begin{center}
\includegraphics[scale=0.35]{skating1Comparison}
\end{center} 

\subsubsection{Skiing}
As with CarDark the new method fails to provide an initial bounding 
box, possibly due to the inusual appearance of the person which is 
also decreasing the detection’s confidence and as a result it gets
dropped in the filtering phase.
\begin{center}
\includegraphics[scale=0.35]{skiingComparison}
\end{center} 



