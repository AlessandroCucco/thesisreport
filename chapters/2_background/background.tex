% set image path
\graphicspath{{./chapters/2_background/images/}}

\chapter{Background}


% ANN
\section{Artificial neural networks}

\subsection{Overview}
\begin{figure}
    \centering
    \begin{subfigure}{0.5\textwidth}
          \includegraphics[width=\textwidth]{ANN}
    \end{subfigure} 
    \caption{An example of an artifical neural network. Source: \cite{ANNGuide}}
\end{figure}

An artificial neural network(ANN) is a computing system inspired by 
the biological neural netork of an animal brain. It learns to
perform tasks by processing a set of examples, rather than being 
programmed as traditional programming. It is composed by units called
neurons and consists of 3 types of layers:
\begin{itemize}
    \item Input layer: these are the inputs of the network.
    \item Hidden layers: these are the layers inbetween the input
          and output layers, and they are responsible for all the
          computation of the neural network.
    \item Output layer: this is the result for the given inputs.
\end{itemize}
The simplest type of ANN is also called a multi layer perceptron(MLP), consisting
of layers of perceptrons which take the output of the previous layer and use it
to compute the input for the following one.
Each layer $i$ implements the following formula:
$$ y_i = \sigma(W_iy_{i-1} + b_i) $$
Where:
\begin{itemize}
    \item $ y_i $: the output of the current layer.
    \item $ \sigma $: the activation function needed to provide non-linearity to
          the system. Typically $tanh$ or $sigmoid$ functions are used.
    \item $ W_i $: weight matrix of the current layer.
    \item $ b_i $: bias vector of the current layer.
    \item $ y_{i-1} $: output of the previous layer or input layer if the current
          layer is the first hiden layer.
\end{itemize}
In the previous image an example of a ANN may be the prediction of
the resting methabolic rate given a person's age, height and weight:
\begin{itemize}
    \item Input layer size: 3, representing age, height and weight.
    \item Output layer size: 1, representing the resting methabolic
          rate.
\end{itemize}

\subsection{Training}
The training process is aimed at improving the accuracy of the model both on training and
test data, according to a predefined metric, the loss function, which computes the difference 
between the 
expected and provided output. One of the most used is the mean squared error(MSE):
it computes the average of the squared difference between the network's 
predictions and the ground truth.
$$ MSE = \frac{1}{n}\sum_{i=1}^n(y_i-\hat{y_i})^2 $$
Where:
\begin{itemize}
    \item $n$: the dataset or batch size.
    \item $y_i$: the ground truth for the example $i$.
    \item $\hat{y_i}$: the network's output for example $i$.
\end{itemize}

More in detail the training is divided into two stages:
\begin{enumerate}
    \item Forward propagation: in this stage the network is provided with input data
          and processes it according to its layers to generate some output.
    \item Back propagation: the output of the network is compared with the ground truth 
          data using the loss function and this error is propagated backwards in the
          network to update the parameters of each layer.
\end{enumerate}

\subsubsection{Forward propagation}
\begin{center}
    \includegraphics[scale=0.5]{ANN}\\
\end{center}
By looking at the previous example of a simple ANN the forward propagation is calculated
as:
$$ y_1 = \sigma(W_1x + b_1) $$
$$ y_2 = \sigma(W_2y_1 + b_2) $$
$$ y_3 = \sigma(W_3y_2 + b_3) $$
Where:
\begin{itemize}
    \item $y_i$: the output at layer $i$. Note that $y_3$ refers to the output of the
          output layer.
    \item $W_i$: weight matrix at layer $i$.
    \item $b_i$: bias vector at layer $i$.
    \item $\sigma$: the activation function. 
\end{itemize}
So we simply compute the output at a given layer and use it as input of the following
one until we reach the output layer.

% CNN
\section{Convolutional Neural Networks}

\subsection{Overview}
\begin{center}
    \includegraphics[scale=0.5]{CNN}
\end{center}
Convolutional Neural Networks(CNN) are a particular class of feedforward
neural networks created for object recognition and classification. It
consists of a set of layers aimed at capturing the features of the objects:
\begin{itemize}
    \item Convolutional layer: it is responsible for the extraction of the
          visual features from the object. The first convolutional layers
          in the pipeline are capable of recognizing straight line and
          edges, whereas the last ones can detect more complex features such 
          as faces, cars and dogs.
    \item ReLU layer: this layer applies a non-linear transformation to the
          output of the previous layer: this is needed since the convolutional
          layer applies a linear transformation, whereas the function we are 
          trying to approximate is non-linear.
    \item Pooling layer: this type of layer is used to reduce the
          dimensionality of the output of a convolution layer, as it pools
          a region of the previous layer's output via a non-linear 
          transformation
    \item Fully connected layer: it computes the probability score of all
          output classes.
\end{itemize}

\subsection{Layers}

% conv layer
\subsubsection{Convolutional layer}
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{convLayer1.png} 
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{convLayer2.png}
    \end{subfigure}
    \caption{An example of a convolution operation. Source: \cite{CNNGuide}}
\end{figure}


The primary purpose of the convolution is to extract visual features from
the input image: this is achieved by sliding a convolution filter over the
image and computing the dot product between the input and the filter at 
every possible sliding position.

Each filter is described by:
\begin{itemize}
    \item width: the width of the filter's receptive field.
    \item height: the height of the filter's receptive field.
    \item stride: the number of pixels by which the filter slides over 
          the input matrix: if it is 1 the filter is moved one pixel at 
          a time, whereas if it is 2 it will jump two pixels at a time
          and so on.
    \item depth: this is the number of filters we want to use for the 
          convolutional operation. Generally we want a relatively high
          number of filters, since each one will capture a different 
          feature, such as a vertical and horizontal line, a corner and
          so on: this allows the network to become better at recognizing 
          patterns in unseen images. 
    \item zero-padding: it specifies whether we want to pad the input
          matrix with zeros around the border. It allows us to control 
          the size of the feature maps and can be classified into:
          \begin{itemize}
              \item wide convolution: apply zero-padding.
              \item narrow convolution: no zero-padding.
          \end{itemize}
\end{itemize}

% activation
\subsubsection{Activation layer}
The activation layer is responsible for providing non-linearity to the
system: several activation functions have been used such as tanh or sigmoid
but nowadays the most used is the Rectified Linear Unit(ReLU), not only
because of its semplicity but mostly because it allows a faster update
of the weights during backpropagation, resulting in a faster training.

The mathematical formula for ReLU is $f(x) = max(0, x)$ and it is applied
to every output of the convolutional layer in standard CNNs.
\begin{figure}
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{ReLU.png} 
    \end{subfigure}
    \caption{An example of the output of a ReLU layer. Source: \cite{CNNGuide}}
\end{figure}

% pooling
\subsubsection{Pooling layer}
Pooling layer is used to reduce the number of paramaters when the previous layer's output is 
too large: as such it reduces the dimensionality but retains the important information. Each 
pooling layer is characterized by a set of paramaters:
\begin{itemize}
    \item height: the height of the pooling window.
    \item width: the width of the pooling window.
    \item stride: the number of pixels by which the pooling window slides over, similar to the
          stride in convolutional layer.
    \item type: the type of pooling to perform:
          \begin{itemize}
              \item Max pooling: keep only the largest element in the pooling window
              \item Average pooling: return the average of all the elements in the pooling 
                    window
              \item Sum pooling: return the sum of all the elements in the pooling window
          \end{itemize}
\end{itemize}
\begin{figure}[!htb]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{pool.png} 
    \end{subfigure}
    \caption{An example of the output of a pool layer. Source: \cite{CNNGuide}}
\end{figure} 
\FloatBarrier

% fully connected
\subsubsection{Fully connected layer}
The last layer in the CNN model is the fully connected layer(FC): it takes the flattened 
feature maps coming from the previous layer and combines them together, passing them to the
very last layer consisting of a softmax or sigmoid layer, used to predict the class
probabilities.
\begin{figure}
    \centering
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{FC.png} 
    \end{subfigure}
    \caption{An example of a fully connected layer. Source: \cite{CNNGuide}}
\end{figure}

\newpage

% RNN
\section{Recurrent Neural Networks}
\subsection{Overview}
\begin{figure}
    \centering
    \begin{subfigure}{0.6\textwidth}
        \includegraphics[width=\textwidth]{RNN.png} 
    \end{subfigure}
    \caption{An example of a recurrent neural network. Source: \cite{RNNGuide}}
\end{figure}
Recurrent neural networks(RNN) are a special kind of artificial neural networks which contain
loops in them, thus allowing information to persist over time. This kind of behaviour is 
extremely useful in speech recognition, image captioning and language modeling, where 
it shines.
Each RNN node at time $t$ receives some input $x_t$ and outputs a value $o_t$, while the loop
allows information to be maintained over time. Moreover each RNN node can be tought as a 
sequence of RNN nodes, each responsible for processing the input at subsequent timesteps, 
passing their internal state $h_t$ to their successor.

\subsection{RNN types} 
\subsubsection{Vanilla RNN}
\begin{figure}
    \centering
    \begin{subfigure}{0.6\textwidth}
        \includegraphics[width=\textwidth]{RNN.png} 
    \end{subfigure}
    \caption{An example of a recurrent neural network. Source: \cite{RNNGuide}}
\end{figure}
Vanilla RNN are the simplest version of a RNN: it receives an input vector $x$ and returns an
output vector $y$, implementing the following formulas: 
$$ h_t = \sigma(U_h x_t + W_h h_{t-1} ) $$
$$ y_t = O h_t $$
where:
\begin{itemize}
    \item $x_t$: input at time $t$.
    \item $y_t$: output at time $t$.
    \item $h_t$: RNN cell's hidden state at time $t$, typically it is initialized to zero.
    \item $h_{t-1}$: RNN cell's hidden state at previous timestep.
    \item $\sigma$: activation function required to implement non-linearity. Typically the 
          $tanh$ function is used, which fixes the activations in the [-1, 1] range.
\end{itemize}
Moreover the RNN is characterized by a set of parameters:
\begin{itemize}
    \item $W_h$: this matrix is responsible for the flow of information between the previous
          and current timestep states.
    \item $U_h$: the role of this matrix is to control the information passed between the 
          current input and the hidden state. 
    \item $O$: this matrix is instead responsible for providing an output given the hidden
          state of the cell.
\end{itemize}

In theory the vanilla RNN should be able to learn long-term dependencies in the input sequence,
but in reality it fails in doing so because of two main reasons:
\begin{itemize}
    \item Exploding gradients: this is due to increasing gradients at each timesteps.
    \item Vanishing gradient: this is caused by the chain rule of multiplication and causes
          gradients to become smaller at every timestep, thus stalling training.
\end{itemize}

% LSTM
\subsubsection{Long Short Term Memory}
\begin{figure}
    \centering
    \begin{subfigure}{0.6\textwidth}
        \includegraphics[width=\textwidth]{LSTM.png} 
    \end{subfigure}
    \caption{An example of a Long Short-Term Memory. Source: \cite{LSTMImage}}
\end{figure}
Long Short Term Memory (LSTM) were introduced by Hochreiter \& Schmidhuber in 1997 \cite{lstmPaper} who  
specifically designed it to avoid the long term dependency problem crippling the vanilla RNN.
The LSTM has a similar flow of information as the vanilla RNN, but the key difference relies
in the computations within the cell: its key components are:
\begin{itemize}
    \item cell state: it controls the relevant information which needs to be memorized over
          the course of the sequence.
    \item gates: they control how information is added or removed onto the cell state and
          they are trained to learn which information is relevant to keep or forget.
\end{itemize}
It is described by the following equations:
$$ f_t = \sigma(W_f\cdot[h_{t-1}, x_t] + b_f) $$
$$ i_t = \sigma(W_i\cdot[h_{t-1}, x_t] + b_i) $$
$$ \tilde{C_t} = tanh(W_c\cdot[h_{t-1}, x_t] + b_c)$$
$$ C_t = f_t*C_{t-1} + i_t*\tilde{C_t}$$
$$ o_t = \sigma(W_o\cdot[h_{t-1}, x_t] + b_o) $$
$$ h_t = o_t*tanh(C_t) $$
Explaining it step by step:
\begin{enumerate}
    \item The first step is to decide which information we need discard from the cell state. 
          This decision is taken by the forget gate $f_t$ by looking at the previous timestep's
          prediction, $h_t$ and the current timestep's input $x_t$.
    \item Next we need to update the state of the cell, this is the role of the input gate
          $i_t$ and the candidate vector $\tilde{C_t}$: the input gate decides the values that
          need to be updated, similarly to the forget gate, wheras the candidate vector
          contains the actual values which need to be written to the cell state.
    \item The final cell state, $C_t$ is then calculated by:
          \begin{itemize}
              \item forget: in this step we multiply the forget gate $f_t$ by the previous
                    cell's state $C_t$, allowing the cell state to forget the things we do not
                    need
              \item update: in this second setp we need to update the cell's state $C_t$ with 
                    the candidate vector $\tilde{C_t}$ in order to update the cell with the new
                    values.
          \end{itemize}
    \item Lastly we need to compute the actual output of the LSTM cell: this is achieved by
          first concatenating the output of the previous timestep $h_{t-1}$ with the current
          timestep input $x_t$ and then applying applying an activation function. Its output
          is then pointwise multiplied with the activation of the current timestep cell state
          $C_t$
\end{enumerate}

% GRU
\subsubsection{Gated Recurrent Unit}
\begin{figure}
    \centering
    \begin{subfigure}{0.6\textwidth}
        \includegraphics[width=\textwidth]{GRU.png} 
    \end{subfigure}
    \caption{An example of a Gated Recurrent Unit. Source: \cite{GRUGuide}}
\end{figure}
Gated Recurrent Unit(GRU) was first developed in 2014 by Kyunghyun Cho et al \cite{GRUPaper}, 
consisting in a similar architecture to the LSTM, being at the same time less complex but able
to achieve similar results.
The main differences with respect to the LSTM are:
\begin{itemize}
    \item No cell state, used in the LSTM to transfer information over time.
    \item It uses only two gates, the reset and update gates.
    \item It is less complex, so it allows faster training and less memory requirements.
    \item It provides similar results to the LSTM, although it depends on the specific 
          applications.
\end{itemize}
It is described by the following equations:
$$ z_t = \sigma(W_z \cdot [h_{t-1}, x_t]) $$
$$ r_t = \sigma(W_r \cdot [h_{t-1}, x_t]) $$
$$ \tilde{h_t} = tanh(W \cdot [r_t*h_{t-1}, x_t]) $$
$$ h_t = (1 - z_t)*h_{t-1} + z_t*\tilde{h_t} $$
More in detail:
\begin{enumerate}
    \item First the update gate $z_t$ determines the information that needs to be propagated
          in the future timesteps: this is achieved by multipling the previous timestep's 
          network prediction $h_{t-1}$ and the current timestep input $x_t$ by the update
          matrix $W_z$, which controls the flow of information in the future.
    \item The reset gate $r_t$ is instead responsible for selecting how much of the past
          information to forget.
    \item Next, the new memory content is computed by $\tilde{h_t}$: first the reset gate 
          $r_t$ is applied to the previous timestep's prediction $h_{t-1}$ to remove unnecessary
          information, then
    \item Lastly the memory output at the current timestep $h_t$ is computed by:
          \begin{itemize}
              \item Multiplying the update gate $z_t$ by the new memory content $\tilde{h_t}$
                    to set the new memory values for the next timestep
              \item Multiplying the opposite of the update gate $1 - z_t$ by the memory content
                    at the previous timestep $h_{t-1}$ to propagate the information on successive
                    timesteps  
          \end{itemize}  
\end{enumerate}

\section{ResNets}
\label{sect:resNets}
Deeper convolutional networks as \cite{VGG} have shown better results than their shallow counterparts. However,
adding more layers to the network allows to reduce the error rate up to a point, past which
their accuracy starts to deteriorate, as the authors of \cite{ResNet50} experimentally concluded.
As such, they proposed to include residual connections, defined as a
shortcut connection summing the output of the residual block with its input, as can be seen 
in figure \ref{fig:resBlock}.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.5\textwidth}
          \includegraphics[width=\textwidth]{resBlock}
    \end{subfigure} 
    \caption{Visualization of the residual block used in \cite{ResNet50}. A residual block 
             consists of 2 cascaded convolutional layers. The block's output is defined as the
             sum between the block's input and the last layer's output. Source: \cite{ResNet50}.}
    \label{fig:resBlock}
\end{figure}
The addition of the residual connection allowed the authors of \cite{ResNet50}
to achieve state of the art results, showing that just by adding residual 
connections, with no additional trainable parameter, allowed deeper models to perform better
than their shallow counterparts, as can be seen in figure \ref{fig:resNetResults}.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.9\textwidth}
          \includegraphics[width=\textwidth]{resNetComparison}
    \end{subfigure} 
    \caption{Training on CIFAR-10. Dashed lines denote training error, whereas bold ones represent testing error.
             Left: plain networks. Right: resNets. Notice how in plain networks both training and 
             validation loss increases with the depth of the network, whereas with resNets deeper networks
             achieve the best results. Source: \cite{ResNet50}.}
    \label{fig:resNetResults}
\end{figure}

Several other papers experimented with residual connections: one example is \cite{ResLSTM},
where they experimented with a residual LSTM network for sentiment classification. They implemented 
their network as a sequence of residual blocks, each of which is defined by 2 consecutive LSTM
cells, showing that residual connections were suited to LSTMs as well. A visualization of the
architecture is available in figure \ref{fig:resLstmNet}.
\begin{figure}[H]
      \centering
      \begin{subfigure}{0.9\textwidth}
            \includegraphics[width=\textwidth]{resLstmNet}
      \end{subfigure} 
      \caption{Visualization of the residual LSTM architecture adopted in \cite{ResLSTM}: 
               a residual block is defined as 2 LSTM connected in sequence, whose output 
			   is summed with its input. Source: \cite{ResLSTM}.}
	  \label{fig:resLstmNet}
\end{figure}

Moreover, in \cite{ResLSTM} they noticed the same behaviour of the networks as in \cite{ResNet50}: 
deeper, non-residual networks performed worse than their shallow counterparts, whereas just
adding skip connections every 2 LSTM layers drastically improved their performance, without
the need for any additional trainable parameter, as can be seen in figure 
\ref{fig:resLstmResults}. 
\begin{figure}[H]
      \centering
      \begin{subfigure}{0.9\textwidth}
            \includegraphics[width=\textwidth]{resLstmResults} 
      \end{subfigure} 
	  \caption{Empirical results of the residual LSTM architecture proposed in \cite{ResLSTM}:
	  		   standard deep LSTM networks perform
               worse than their shallow counterparts, whereas the reverse applies with residual
               connections. Note the simmetry of these results with the ones provided 
			   by \cite{ResNet50}. Source: \cite{ResLSTM}.}
	  \label{fig:resLstmResults}
\end{figure}

\section{DenseNets}
\label{sect:denseNets}
Recently a new paper, titled DenseNets \cite{DenseNet}, aimed at improving CNNs was released: it 
substantially improved the state of the art, providing even better results than ResNets 
\cite{ResNet50}. This is achieved through the usage of dense blocks: each of these blocks 
consists of a sequence of convolutional layers, whose each input is defined as the
concatenation of all previous layers' outputs. A visualization of the dense block is 
available in figure \ref{fig:denseNetBlock}.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.6\textwidth}
		  \includegraphics[width=\textwidth]{denseNetBlock} 
	\end{subfigure} 
	\caption{Visualization of the dense block used in \cite{DenseNet}: each block is 
			 composed of several convolutional layers, each of which takes as input all 
			 preceding feature maps. Source: \cite{DenseNet}}
	\label{fig:denseNetBlock}
\end{figure}

As already cited before, they showed competitive results with other state of the art image 
classifiers, most notably ResNets \cite{ResNet50}. In particular, as we can see in figure 
\ref{fig:denseNetResults}, DenseNets showed a lower validation error than ResNets at the 
same parameter count, or equivalently, a lower number of parameters for the same result.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.9\textwidth}
		  \includegraphics[width=\textwidth]{denseNetResults}
	\end{subfigure} 
	\caption{Visualization of the difference between ResNets \cite{ResNet50} and DenseNets
			 \cite{DenseNet}: number of parameters and validation errors are showed on the
			 horizontal and vertical axes, respectively. Note how DenseNets achieve the same
			 validation error at a lower number of parameters and better results at the same
			 model size. Source: \cite{DenseNet}.}
	\label{fig:denseNetResults}
\end{figure}

Other authors experimented with dense networks: one example is \cite{denseRnn}, where they 
implemented a densely connected RNN network for language modelling, a subset task of natural
language processing(NLP). Their architecture consists of a set of RNN layers, each receiving 
as input the concatenation between the output of the previous layers and the input layer. The 
dense connections are not only applied to the RNN layers, but to the last FC layer as well. A 
visualization of the described architecture is available ain figure \ref{fig:denseRnnNet}.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.9\textwidth}
		  \includegraphics[width=\textwidth]{denseRnnNet}
	\end{subfigure} 
      \caption{Visualization of the architecture used in \cite{denseRnn}: each layer in the
               network receives as input the concatenation of the input layer and the output
               of all previous layers. Source: \cite{denseRnn}.}
	\label{fig:denseRnnNet}
\end{figure}

The evaluation of their densely connected rnn network on the PTB language modelling task
is available in figure \ref{fig:denseRnnResults}: their network achieves the best validation
and testing accuracy with respect to stacked LSTM at the same number of parameters, and 
further improves as the number of parameters is increased.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.9\textwidth}
		  \includegraphics[width=\textwidth]{denseRnnResults} 
	\end{subfigure} 
      \caption{Evaluation of densely connected RNN for the PTB language modelling task.
               Source: \cite{denseRnn}}
	\label{fig:denseRnnResults}
\end{figure}

\newpage