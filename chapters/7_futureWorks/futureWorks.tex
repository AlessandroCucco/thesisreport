\graphicspath{{./chapters/7_futureWorks/images/}}

\chapter{Future works}
In this chapter we will discuss about additional experiments which have not been
completed due to timing reasons. We will explain how to integrate them with the 
current architecture, as well as providing an explanation for their possible benefits.

\section{Loss change}
The current training procedure uses Mean Absolute Error(MAE) as the loss function, 
defined in equation \ref{eq:maeLoss}.

\begin{equation}
      MAE = \frac{\sum_{i=1}^{n}\mid y_i - y_i^p\mid}{n}
      \label{eq:maeLoss}
\end{equation}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.7\textwidth}
          \includegraphics[width=\textwidth]{mae.png} 
    \end{subfigure}
    \caption{MAE loss(y axis) vs prediction(x axis). Source:\cite{lossGuide}}
    \label{fig:maeLoss}
\end{figure}

It computes the average absolute error between the ground truth value $y_i$ and the 
network's prediction $y_i^p$ for all predictions of the model in the batch of size $n$, thus 
allowing to have a stable learning even in the presence of outliers, which cause a 
high loss value for a particular example. Unfortunately, its mathematical formula causes
it to have a constant gradient even at low loss values, causing unstable learning, even 
at low learning rate values: this produces an unstable training process, potentially
leading the model not to converge to the local or global minimum.

One solution may be the usage of Mean Squared Error(MSE) as the loss function, defined in 
equation \ref{eq:mseLoss}.

\begin{equation}
      MSE = \frac{\sum_{i=1}^{n}(y_i - y_i^p)^2}{n}
      \label{eq:mseLoss}
\end{equation}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.7\textwidth}
          \includegraphics[width=\textwidth]{mse.png} 
    \end{subfigure}
    \caption{MSE loss(y axis) vs prediction(x axis). Source:\cite{lossGuide}}
    \label{fig:mseLoss}
\end{figure}
It computes the average of the squared difference between the ground truth value $y_i$
and predicted value $y_i^p$ for all examples in the batch of size $n$.
As such, it allows to have very small gradients at low loss values, which cause the training
to converge even in the presence of high learning rate values. On the other hand, however, it
is extremely sensible to outliers, since their error is squared and as a consequence they 
prevail over low error training examples.

\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{maeVsMse.png} 
    \end{subfigure}
    \caption{Comparison between the MSE and MAE losses. Notice how MSE converges to a stable
             solution, whereas MAE leads to an unstable solution. Source:\cite{lossGuide}}
    \label{fig:maeVsMse}
\end{figure}

Ideally, we would like to combine the robustness to outliers typical of MAE with the 
convergence properties of MSE, without any of their drawbacks discussed before. That is why
we propose an experiment with the Huber loss, defined in equation \ref{eq:huberLoss}

\begin{equation}
      L_{\delta}(y, f(x)) =
      \begin{cases}
        \frac{1}{2}(y - f(x))^2 & \text{for $|y - f(x)| \leq \delta$} \\
        \delta |y - f(x)| - \frac{1}{2}\delta^2& \text{otherwise}
      \end{cases}
      \label{eq:huberLoss}
\end{equation}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.7\textwidth}
          \includegraphics[width=\textwidth]{huberLoss.png} 
    \end{subfigure}
    \caption{Huber loss(y axis) vs prediction(x axis), the color indicates the delta values.
             Source:\cite{lossGuide}}
    \label{fig:huberLoss}
\end{figure}

As we can see from figure \ref{fig:huberLoss} the loss curve behaves linearly at high values,
but quadratically at low ones: as such it combines the benefits of both MAE and MSE.
However it requires an additional hyperparameter, $\delta$, which identifies the value past
which the loss function behaves linearly, which must be set accordingly to properly balance 
the effect of the outliers. 

We suggest that this experiment should provide us with better solutions and a more stable
training, since when the loss approaches zero its gradient scales accordingly as well, 
while still being robust to outliers, at the cost of a slightly longer training time, as 
the smaller the error, the smaller the gradient, and as a consequence the smaller the step size
taken in each training update.

\section{Changing the CNN}
The Re\textsuperscript{3} architecture originally uses AlexNet\cite{AlexNet} as CNN: a possible 
improvement both in terms of accuracy, robustness and run time may be achieved by substituting
it with a more recent and efficient architecture. In particular, we suggest experimenting with:
\begin{itemize}
    \item MobileNet \cite{MobileNet}
    \item ResNet50 \cite{ResNet50}
    \item InceptionV3 \cite{InceptionV3}
\end{itemize} 
We opted for these models since they offer an interesting tradeoff between accuracy, computational cost
and model size, as it can be seen in figure \ref{fig:cnnComparison}.
Both their code and pretrained weights are available in Tf-slim \cite{TF-slim}, a lightweight 
library containing a set of CNN architectures, similar to the keras.application library 
\cite{kerasApplication}, which allows both easier prototyping and reproducibility.
\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
          \includegraphics[width=\textwidth]{CNNComparison} 
    \end{subfigure}
    \caption{CNN architecture comparison. Source:\cite{CNNAnalysis}}
    \label{fig:cnnComparison}
\end{figure}

\section{Additional datasets}
For training Re\textsuperscript{3} 2 datasets have been used:
\begin{itemize}
    \item Imagenet DET \cite{ILSVRC15}: it consists of a set of images, each labeled by a 
          bounding box.
    \item Imagenet VID \cite{ILSVRC15}: it consists of about 86 GB of data divided into frames 
          and annotation files. It is the most vast training dataset for object tracking, 
          although it only consists of 30 object categories.
\end{itemize}

We suggest experimenting with additional datasets, to introduce the tracker to new object 
classes and transformation, such as:
\begin{itemize}
    \item ALOV \cite{ALOV}: it is composed of several short videos, ranging from easy to
          difficult. In total it consists of 315 video sequences, with the main source of data
          being YouTube. The tracked objects belong to a variety of categories, from which
          human face, person, ball and fish.
    \item Youtube-BB \cite{YouTubeBB}: it consists of 380000 video segments about 19 seconds long,
          with object belonging to a subset of the classes of the COCO lable set.
\end{itemize}

Unfortunately training datasets have several drawbacks, such as high production costs, 
imperfect accuracy, limited object classes and potentially wrong data, due to manual 
labelling. To mitigate this issues, we propose to generate video sequences not from still 
images as in Re\textsuperscript{3} \cite{Re3}, but from game engines such as Unity 
\cite{Unity} this method provides the highest flexibility possible, allowing us to generate
multiple video sequences from the same example by just varying the camera position and angle,
as well as properly control various attributes, such as illumination variation, background 
clutter and occlusion, just to name a few.

\section{No warping}
In the Re\textsuperscript{3} architecture crops are extracted from the frames and are warped to 
fit the CNN input size. However this may cause excessive distorsion in the tracked object,
causing the CNN to provide the following layers with incorrect or noisy features. This is 
particularly noticeable if objects are very thin and tall or very thick and short: in these cases
the objects are warped to be a square image of 227x277 pixels of size, distorting their original 
aspect ratio as can be see in figure \ref{fig:scaleWarpComparison}.

\begin{figure}[H]
      \centering
      \begin{subfigure}{0.1\textwidth}
            \includegraphics[width=\textwidth]{cropImage} 
      \end{subfigure}
      \begin{subfigure}{0.27\textwidth}
            \includegraphics[width=\textwidth]{cropImageWarp} 
      \end{subfigure}
      \begin{subfigure}{0.27\textwidth} 
            \includegraphics[width=\textwidth]{cropImageScale} 
      \end{subfigure}
      \caption{Comparison between scaling and warping techniques: on the left the cropped image,
               in the middle the warped crop of Re\textsuperscript{3}, while on the right our 
               proposed crop scaling method. Note how the aspect ratio of the tracked object is 
               manitained in the proposed method, whereas in Re\textsuperscript{3} the object 
               is heavily distorted.}
      \label{fig:scaleWarpComparison}
  \end{figure}

As such, we propose an additional experiment, by taking the crop of the frame and resizing it to
the input shape of the CNN, without warping, by filling the remaining space of the image with 
either black, grey or the same color as the crop's edge. This change may allow us to keep the CNN
weights fixed without any penalty, since it does not need to learn new features for the warped 
images as in the original architecture.
% TODO add pictures for better explanation

\section{Snapshot ensembling}
One of the main limitations of the current training protocol is that the learning rate is reduced
by hand after noticing a plateau: while it allows us to get closer to the local minimum it may 
miss other better local minima, or worse, the global minimum. To fix this problem we suggest 
implementing a training curriculum similar to \cite{SnapshotEnsembling}, following the following
training protocol for each unroll:
\begin{itemize}
      \item Select the best model of the previous training unroll.
      \item Train it with an initial learning rate and decrease it over time.
      \item Save the model at the end of the previous point.
      \item Repeat the previous 2 points $n$ times.
\end{itemize}

\begin{figure}[H]
      \centering
      \begin{subfigure}{\textwidth}
            \includegraphics[width=\textwidth]{SnapshotEnsembling} 
      \end{subfigure}
      \caption{Visualization of the proposed training protocol. Source:\cite{SnapshotEnsembling}}
  \end{figure}

This training curriculum allows us to better navigate through the solution space of a model, 
by just training one and getting $n$ for free, from which we can select the best one to further
proceed with training or evaluate it on the test set.